﻿using FFX_HD_Save_Editor.Views;
using System.Windows;

namespace FFX_HD_Save_Editor
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            new ExceptionViewer("An unrecoverable error has occurred", e.Exception, Application.Current.MainWindow).ShowDialog();

            e.Handled = true;

            Application.Current.Shutdown(1);
        }
    }
}
