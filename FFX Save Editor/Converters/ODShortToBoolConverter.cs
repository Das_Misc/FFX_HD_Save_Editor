﻿using Shared_Classes.TaskbarNotification;
using System;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Data;

namespace FFX_HD_Save_Editor.Converters
{
    public class ODShortToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is short)
            {
                if ((short)value == 0)
                    return true;
                else
                    return false;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is bool)
            {
                if ((bool)value)
                    return 0;
                else
                {
                    using (Stream iconStream = Application.GetResourceStream(new Uri("pack://application:,,,/Resources/icon_FFX.ico")).Stream)
                    {
                        TaskbarIcon tbi = new TaskbarIcon();
                        tbi.Icon = new Icon(iconStream);
                        tbi.ShowBalloonTip("Final Fantasy X HD Save Editor", "The value should not be 0!", BalloonIcon.Warning);
                        tbi.TrayBalloonTipClosed += tbi_TrayBalloonTipClosed;
                    }
                }
            }

            return null;
        }

        private void tbi_TrayBalloonTipClosed(object sender, RoutedEventArgs e)
        {
            (sender as TaskbarIcon).Dispose();
        }
    }
}
