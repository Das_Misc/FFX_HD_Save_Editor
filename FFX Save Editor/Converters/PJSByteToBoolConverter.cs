﻿using System;
using System.Windows.Data;

namespace FFX_HD_Save_Editor.Converters
{
    public class PJSByteToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is byte)
            {
                if ((byte)value == 0x5E)
                    return true;
                else
                    return false;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is bool)
            {
                if ((bool)value)
                    return 0x5E;
                else
                    return 0x40;
            }

            return null;
        }
    }
}
