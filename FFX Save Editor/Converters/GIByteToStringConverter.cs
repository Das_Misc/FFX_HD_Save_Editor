﻿using FFX_HD_Save_Editor.Models;
using System;
using System.Windows.Data;

namespace FFX_HD_Save_Editor.Converters
{
    public class GIByteToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is byte)
            {
                ListItem itemList = new ListItem();

                if ((byte)value == 0xFF)
                    return itemList[itemList.Count - 1];
                else
                    return itemList[(byte)value];
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is string)
            {
                ListItem itemList = new ListItem();

                if (itemList.IndexOf(value.ToString()) == itemList.Count - 1)
                    return 0xFF;
                else
                    return itemList.IndexOf(value.ToString());
            }

            return null;
        }
    }
}
