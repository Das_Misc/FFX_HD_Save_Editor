﻿using System;
using System.Windows.Data;

namespace FFX_HD_Save_Editor.Converters
{
    public class CBStringToImageStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is string)
            {
                if (value.ToString() == "Tidus")
                    return "/Resources/avatar_Tidus.png";
                else if (value.ToString() == "Yuna")
                    return "/Resources/avatar_Yuna.png";
                else if (value.ToString() == "Auron")
                    return "/Resources/avatar_Auron.png";
                else if (value.ToString() == "Kimahri")
                    return "/Resources/avatar_Kimahri.png";
                else if (value.ToString() == "Wakka")
                    return "/Resources/avatar_Wakka.png";
                else if (value.ToString() == "Lulu")
                    return "/Resources/avatar_Lulu.png";
                else if (value.ToString() == "Rikku")
                    return "/Resources/avatar_Rikku.png";
                else if (value.ToString() == "Valefor")
                    return "/Resources/seal_Valefor.png";
                else if (value.ToString() == "Ifrit")
                    return "/Resources/seal_Ifrit.png";
                else if (value.ToString() == "Ixion")
                    return "/Resources/seal_Ixion.png";
                else if (value.ToString() == "Shiva")
                    return "/Resources/seal_Shiva.png";
                else if (value.ToString() == "Bahamut")
                    return "/Resources/seal_Bahamut.png";
                else if (value.ToString() == "Anima")
                    return "/Resources/seal_Anima.png";
                else if (value.ToString() == "Yojimbo")
                    return "/Resources/seal_Yojimbo.png";
                else if (value.ToString() == "Cindy")
                    return "/Resources/seal_Cindy.png";
                else if (value.ToString() == "Sandy")
                    return "/Resources/seal_Sandy.png";
                else if (value.ToString() == "Mindy")
                    return "/Resources/seal_Mindy.png";
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
