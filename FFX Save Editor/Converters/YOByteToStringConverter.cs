﻿using FFX_HD_Save_Editor.Models;
using System;
using System.Windows.Data;

namespace FFX_HD_Save_Editor.Converters
{
    public class YOByteToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is byte)
            {
                FFXSaveInfo ffxsi = new FFXSaveInfo();

                return ffxsi.YojimboOptions[(byte)value];
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is string)
            {
                FFXSaveInfo ffxsi = new FFXSaveInfo();

                for (int i = 0; i < ffxsi.YojimboOptions.Length; i++)
                    if (ffxsi.YojimboOptions[i] == value.ToString())
                        return i;
            }

            return null;
        }
    }
}
