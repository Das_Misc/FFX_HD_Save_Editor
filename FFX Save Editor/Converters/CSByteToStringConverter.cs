﻿using FFX_HD_Save_Editor.Models;
using System;
using System.Windows.Data;

namespace FFX_HD_Save_Editor.Converters
{
    public class CSByteToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is byte)
            {
                ListCactuarStones csList = new ListCactuarStones();

                if ((byte)value < 0x10)
                    return csList[(byte)value];
                else if ((byte)value == 0x19)
                    return csList[0x10];
                else
                    return csList[(byte)value + 1];
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is string)
            {
                ListCactuarStones csList = new ListCactuarStones();

                byte csStage = (byte)csList.IndexOf(value.ToString());

                if (csStage == 0x10)
                    csStage = 0x19;
                else if (csStage > 0x10)
                    csStage--;
                
                return csStage;
            }

            return null;
        }
    }
}
