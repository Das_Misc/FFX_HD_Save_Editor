﻿using System;
using System.Windows.Data;

namespace FFX_HD_Save_Editor.Converters
{
    public class BBUIntToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is uint)
            {
                if ((uint)value == (0xFEFFFF7FL * 2) - 0x100000000)
                    return true;
                else
                    return false;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is bool)
            {
                if ((bool)value)
                    return (0xFEFFFF7FL * 2) - 0x100000000;
                else
                    return 0;
            }

            return null;
        }
    }
}
