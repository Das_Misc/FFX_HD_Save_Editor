﻿using System;
using System.Windows.Data;
using System.Windows.Media;

namespace FFX_HD_Save_Editor.Converters
{
    public class TBBoolToBrushColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is bool)
            {
                if ((bool)value)
                    return new SolidColorBrush(Colors.Black);
                else
                    return new SolidColorBrush(Colors.Gray);
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
