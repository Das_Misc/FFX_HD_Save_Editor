﻿using System;
using System.Windows;
using System.Windows.Data;

namespace FFX_HD_Save_Editor.Converters
{
    public class StringToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is string)
            {
                if (value.ToString() == "Tidus")
                    return Visibility.Hidden;
                else
                    return Visibility.Visible;
            }
            else if (value == null)
                return Visibility.Visible;

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
