﻿using System;
using System.Windows.Data;

namespace FFX_HD_Save_Editor.Converters
{
    public class ByteToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is byte)
            {
                if ((byte)value == 1)
                    return true;
                else
                    return false;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is bool)
            {
                if ((bool)value)
                    return 1;
                else
                    return 0;
            }

            return null;
        }
    }
}
