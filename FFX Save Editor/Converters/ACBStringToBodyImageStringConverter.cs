﻿using System;
using System.Windows.Data;

namespace FFX_HD_Save_Editor.Converters
{
    public class ACBStringToBodyImageStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is string)
            {
                if (value.ToString() == "Valefor")
                    return "/Resources/body_Valefor.png";
                else if (value.ToString() == "Ifrit")
                    return "/Resources/body_Ifrit.png";
                else if (value.ToString() == "Ixion")
                    return "/Resources/body_Ixion.png";
                else if (value.ToString() == "Shiva")
                    return "/Resources/body_Shiva.png";
                else if (value.ToString() == "Bahamut")
                    return "/Resources/body_Bahamut.png";
                else if (value.ToString() == "Anima")
                    return "/Resources/body_Anima.png";
                else if (value.ToString() == "Yojimbo")
                    return "/Resources/body_Yojimbo.png";
                else if (value.ToString() == "Cindy")
                    return "/Resources/body_Cindy.png";
                else if (value.ToString() == "Sandy")
                    return "/Resources/body_Sandy.png";
                else if (value.ToString() == "Mindy")
                    return "/Resources/body_Mindy.png";
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
