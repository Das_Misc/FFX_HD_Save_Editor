﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace FFX_HD_Save_Editor.Converters
{
    public class StringToBoolConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is string)
            {
                if (value.ToString() == "Tidus" || value.ToString() == "Auron" || value.ToString() == "Kimahri" || value.ToString() == "Wakka")
                    return true;
                else
                    return false;
            }
            else if (value == null)
                return true;

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
