﻿using System.ComponentModel;

namespace FFX_HD_Save_Editor.Models
{
    public class GameItem : INotifyPropertyChanged
    {
        private byte item, quantity;

        public byte Item
        {
            get { return item; }
            set
            {
                if (Item != value)
                {
                    item = value;
                    NotifyPropertyChanged("Item");
                }
            }
        }

        public byte Quantity
        {
            get { return quantity; }
            set
            {
                if (Quantity != value)
                {
                    quantity = value;
                    NotifyPropertyChanged("Quantity");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
