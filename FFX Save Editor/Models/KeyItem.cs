﻿using System.ComponentModel;

namespace FFX_HD_Save_Editor.Models
{
    public class KeyItem : INotifyPropertyChanged
    {
        private bool bit;
        private string item;

        public bool Bit
        {
            get { return bit; }
            set
            {
                if (Bit != value)
                {
                    bit = value;
                    NotifyPropertyChanged("Bit");
                }
            }
        }

        public string Item
        {
            get { return item; }
            set
            {
                if (Item != value)
                {
                    item = value;
                    NotifyPropertyChanged("Item");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
