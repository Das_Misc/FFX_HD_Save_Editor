﻿using System;
using System.ComponentModel;
using System.Linq;

namespace FFX_HD_Save_Editor.Models
{
    internal class FFXSaveInfo : INotifyPropertyChanged
    {
        private bool maximizeStatNodes, saveLoaded;
        private bool arenaMonstersUnlock, newGamePlus, wobblyChocoboCompleted, dodgerChocoboCompleted, hyperDodgerChocoboCompleted, catcherChocoboCompleted;
        private bool addUltima, addNirvana, addMasamune, addLonginus, addWorld, addOnion, addGodhand;
        private uint battles, gil, tidusODUsed;
        private float gilDonated;
        private byte animaUnlock, beatLucaGoers, cactuarSpheres, cactuarStage, celestials, performedJechtShot, sphereGridType, unlockedWorldChampion, yojimboCompatibility, yojimboOption;
        private byte wobblyChocoboSecs, wobblyChocoboMins, wobblyChocoboMils, dodgerChocoboSecs, dodgerChocoboMins, dodgerChocoboMils;
        private byte hyperDodgerChocoboSecs, hyperDodgerChocoboMins, hyperDodgerChocoboMils, catcherChocoboSecs, catcherChocoboMins, catcherChocoboMils;
        private byte[] sphereGridContents;
        private ushort dodgedLightningsInARow, hitByLightnings;
        private BlitzPlayer tidusB = new BlitzPlayer(), wakkaB = new BlitzPlayer(), dattoB = new BlitzPlayer(), lettyB = new BlitzPlayer(), jassuB = new BlitzPlayer(), bottaB = new BlitzPlayer(), keepaB = new BlitzPlayer();
        private BlitzPlayer selectedPlayer;
        private Character auron = new Character(), kimahri = new Character(), lulu = new Character(), rikku = new Character(), tidus = new Character(), wakka = new Character(), yuna = new Character();
        private Character selectedCharacter;
        private Aeon valefor = new Aeon(), ifrit = new Aeon(), ixion = new Aeon(), shiva = new Aeon(), bahamut = new Aeon(), anima = new Aeon(), yojimbo = new Aeon(), cindy = new Aeon(), sandy = new Aeon(), mindy = new Aeon();
        private Aeon selectedAeon;
        
        public string[] Characters
        {
            get { return new string[] { "Tidus", "Yuna", "Auron", "Kimahri", "Wakka", "Lulu", "Rikku" }; }
        }

        public string[] Aeons
        {
            get { return new string[] { "Valefor", "Ifrit", "Ixion", "Shiva", "Bahamut", "Anima", "Yojimbo", "Cindy", "Sandy", "Mindy" }; }
        }

        public string[] Players
        {
            get { return new string[] { "Tidus", "Wakka", "Datto", "Letty", "Jassu", "Botta", "Keepa" }; }
        }

        public string[] YojimboOptions
        {
            get { return new string[] { "To train as a summoner.", "To gain the power to destroy fiends.", "To defeat the most powerful of enemies." }; }
        }

        public bool MaximizeStatNodes
        {
            get { return maximizeStatNodes; }
            set
            {
                if (MaximizeStatNodes == value) return;
                maximizeStatNodes = value;
                NotifyPropertyChanged("MaximizeStatNodes");
            }
        }

        public bool SaveLoaded
        {
            get { return saveLoaded; }
            set
            {
                if (SaveLoaded == value) return;
                saveLoaded = value;
                NotifyPropertyChanged("SaveLoaded");
            }
        }

        public bool ArenaMonstersUnlock
        {
            get { return arenaMonstersUnlock; }
            set
            {
                if (ArenaMonstersUnlock == value) return;
                arenaMonstersUnlock = value;
                NotifyPropertyChanged("ArenaMonstersUnlock");
            }
        }

        public byte AnimaUnlock
        {
            get { return animaUnlock; }
            set
            {
                if (AnimaUnlock != value)
                {
                    animaUnlock = value;
                    NotifyPropertyChanged("AnimaUnlock");
                }
            }
        }

        public byte CactuarSpheres
        {
            get { return cactuarSpheres; }
            set
            {
                if (CactuarSpheres != value)
                {
                    cactuarSpheres = value;
                    NotifyPropertyChanged("CactuarSpheres");
                }
            }
        }

        public byte CactuarStage
        {
            get { return cactuarStage; }
            set
            {
                if (CactuarStage != value)
                {
                    cactuarStage = value;
                    NotifyPropertyChanged("CactuarStage");
                }
            }
        }

        public byte Celestials
        {
            get { return celestials; }
            set
            {
                if (Celestials == value) return;
                celestials = value;
                NotifyPropertyChanged("Celestials");
            }
        }

        public bool NewGamePlus
        {
            get { return newGamePlus; }
            set
            {
                if (NewGamePlus == value) return;
                newGamePlus = value;
                NotifyPropertyChanged("NewGamePlus");
            }
        }

        public bool WobblyChocoboCompleted
        {
            get { return wobblyChocoboCompleted; }
            set
            {
                if (WobblyChocoboCompleted == value) return;
                wobblyChocoboCompleted = value;
                NotifyPropertyChanged("WobblyChocoboCompleted");
            }
        }

        public bool DodgerChocoboCompleted
        {
            get { return dodgerChocoboCompleted; }
            set
            {
                if (DodgerChocoboCompleted == value) return;
                dodgerChocoboCompleted = value;
                NotifyPropertyChanged("DodgerChocoboCompleted");
            }
        }

        public bool HyperDodgerChocoboCompleted
        {
            get { return hyperDodgerChocoboCompleted; }
            set
            {
                if (HyperDodgerChocoboCompleted == value) return;
                hyperDodgerChocoboCompleted = value;
                NotifyPropertyChanged("HyperDodgerChocoboCompleted");
            }
        }

        public bool CatcherChocoboCompleted
        {
            get { return catcherChocoboCompleted; }
            set
            {
                if (CatcherChocoboCompleted == value) return;
                catcherChocoboCompleted = value;
                NotifyPropertyChanged("CatcherChocoboCompleted");
            }
        }

        public byte CatcherChocoboMils
        {
            get { return catcherChocoboMils; }
            set
            {
                if (CatcherChocoboMils == value) return;
                catcherChocoboMils = value;
                NotifyPropertyChanged("CatcherChocoboMils");
            }
        }

        public byte CatcherChocoboMins
        {
            get { return catcherChocoboMins; }
            set
            {
                if (CatcherChocoboMins == value) return;
                catcherChocoboMins = value;
                NotifyPropertyChanged("CatcherChocoboMins");
            }
        }

        public byte CatcherChocoboSecs
        {
            get { return catcherChocoboSecs; }
            set
            {
                if (CatcherChocoboSecs == value) return;
                catcherChocoboSecs = value;
                NotifyPropertyChanged("CatcherChocoboSecs");
            }
        }

        public bool AddUltima
        {
            get { return addUltima; }
            set
            {
                if (AddUltima == value) return;
                addUltima = value;
                NotifyPropertyChanged("AddUltima");
            }
        }

        public bool AddNirvana
        {
            get { return addNirvana; }
            set
            {
                if (AddNirvana == value) return;
                addNirvana = value;
                NotifyPropertyChanged("AddNirvana");
            }
        }

        public bool AddMasamune
        {
            get { return addMasamune; }
            set
            {
                if (AddMasamune == value) return;
                addMasamune = value;
                NotifyPropertyChanged("AddMasamune");
            }
        }

        public bool AddLonginus
        {
            get { return addLonginus; }
            set
            {
                if (AddLonginus == value) return;
                addLonginus = value;
                NotifyPropertyChanged("AddLonginus");
            }
        }

        public bool AddWorld
        {
            get { return addWorld; }
            set
            {
                if (AddWorld == value) return;
                addWorld = value;
                NotifyPropertyChanged("AddWorld");
            }
        }

        public bool AddOnion
        {
            get { return addOnion; }
            set
            {
                if (AddOnion == value) return;
                addOnion = value;
                NotifyPropertyChanged("AddOnion");
            }
        }

        public bool AddGodhand
        {
            get { return addGodhand; }
            set
            {
                if (AddGodhand == value) return;
                addGodhand = value;
                NotifyPropertyChanged("AddGodhand");
            }
        }

        public ushort DodgedLightningsInARow
        {
            get { return dodgedLightningsInARow; }
            set
            {
                if (DodgedLightningsInARow != value)
                {
                    dodgedLightningsInARow = value;
                    NotifyPropertyChanged("DodgedLightningsInARow");
                }
            }
        }

        public byte DodgerChocoboMils
        {
            get { return dodgerChocoboMils; }
            set
            {
                if (DodgerChocoboMils == value) return;
                dodgerChocoboMils = value;
                NotifyPropertyChanged("DodgerChocoboMils");
            }
        }

        public byte DodgerChocoboMins
        {
            get { return dodgerChocoboMins; }
            set
            {
                if (DodgerChocoboMins == value) return;
                dodgerChocoboMins = value;
                NotifyPropertyChanged("DodgerChocoboMins");
            }
        }

        public byte DodgerChocoboSecs
        {
            get { return dodgerChocoboSecs; }
            set
            {
                if (DodgerChocoboSecs == value) return;
                dodgerChocoboSecs = value;
                NotifyPropertyChanged("DodgerChocoboSecs");
            }
        }

        public uint Battles
        {
            get { return battles; }
            set
            {
                if (Battles != value)
                {
                    battles = value;
                    NotifyPropertyChanged("Battles");
                }
            }
        }

        public uint Gil
        {
            get { return gil; }
            set
            {
                if (Gil != value)
                {
                    gil = value;
                    NotifyPropertyChanged("Gil");
                }
            }
        }

        public float GilDonated
        {
            get { return gilDonated; }
            set
            {
                if (GilDonated == value) return;
                gilDonated = value;
                NotifyPropertyChanged("GilDonated");
            }
        }

        public uint TidusODUsed
        {
            get { return tidusODUsed; }
            set
            {
                if (TidusODUsed != value)
                {
                    tidusODUsed = value;
                    NotifyPropertyChanged("TidusODUsed");
                }
            }
        }

        public ushort HitByLightnings
        {
            get { return hitByLightnings; }
            set
            {
                if (HitByLightnings != value)
                {
                    hitByLightnings = value;
                    NotifyPropertyChanged("HitByLightnings");
                }
            }
        }

        public byte HyperDodgerChocoboMins
        {
            get { return hyperDodgerChocoboMins; }
            set
            {
                if (HyperDodgerChocoboMins == value) return;
                hyperDodgerChocoboMins = value;
                NotifyPropertyChanged("HyperDodgerChocoboMins");
            }
        }

        public byte HyperDodgerChocoboMils
        {
            get { return hyperDodgerChocoboMils; }
            set
            {
                if (HyperDodgerChocoboMils == value) return;
                hyperDodgerChocoboMils = value;
                NotifyPropertyChanged("HyperDodgerChocoboMils");
            }
        }

        public byte HyperDodgerChocoboSecs
        {
            get { return hyperDodgerChocoboSecs; }
            set
            {
                if (HyperDodgerChocoboSecs == value) return;
                hyperDodgerChocoboSecs = value;
                NotifyPropertyChanged("HyperDodgerChocoboSecs");
            }
        }

        public byte BeatLucaGoers
        {
            get { return beatLucaGoers; }
            set
            {
                if (BeatLucaGoers == value) return;
                beatLucaGoers = value;
                NotifyPropertyChanged("BeatLucaGoers");
            }
        }

        public byte PerformedJechtShot
        {
            get { return performedJechtShot; }
            set
            {
                if (PerformedJechtShot == value) return;
                performedJechtShot = value;
                NotifyPropertyChanged("PerformedJechtShot");
            }
        }

        public byte SphereGridType
        {
            get { return sphereGridType; }
            set
            {
                if (SphereGridType == value) return;
                sphereGridType = value;
                NotifyPropertyChanged("SphereGridType");
            }
        }

        public byte UnlockedWorldChampion
        {
            get { return unlockedWorldChampion; }
            set
            {
                if (UnlockedWorldChampion == value) return;
                unlockedWorldChampion = value;
                NotifyPropertyChanged("UnlockedWorldChampion");
            }
        }

        public byte YojimboCompatibility
        {
            get { return yojimboCompatibility; }
            set
            {
                if (YojimboCompatibility == value) return;
                yojimboCompatibility = value;
                NotifyPropertyChanged("YojimboCompatibility");
            }
        }

        public byte YojimboOption
        {
            get { return yojimboOption; }
            set
            {
                if (YojimboOption == value) return;
                yojimboOption = value;
                NotifyPropertyChanged("YojimboOption");
            }
        }

        public byte WobblyChocoboMils
        {
            get { return wobblyChocoboMils; }
            set
            {
                if (WobblyChocoboMils == value) return;
                wobblyChocoboMils = value;
                NotifyPropertyChanged("WobblyChocoboMils");
            }
        }

        public byte WobblyChocoboMins
        {
            get { return wobblyChocoboMins; }
            set
            {
                if (WobblyChocoboMins == value) return;
                wobblyChocoboMins = value;
                NotifyPropertyChanged("WobblyChocoboMins");
            }
        }

        public byte WobblyChocoboSecs
        {
            get { return wobblyChocoboSecs; }
            set
            {
                if (WobblyChocoboSecs == value) return;
                wobblyChocoboSecs = value;
                NotifyPropertyChanged("WobblyChocoboSecs");
            }
        }

        public byte[] SphereGridContents
        {
            get { return sphereGridContents; }
            set
            {
                if (SphereGridContents != null && Enumerable.SequenceEqual(SphereGridContents, value)) return;
                sphereGridContents = value;
                NotifyPropertyChanged("SphereGridContents");
            }
        }

        public BlitzPlayer TidusB
        {
            get { return tidusB; }
            set
            {
                if (TidusB == value) return;
                tidusB = value;
                NotifyPropertyChanged("TidusB");
            }
        }

        public BlitzPlayer WakkaB
        {
            get { return wakkaB; }
            set
            {
                if (WakkaB != value)
                {
                    wakkaB = value;
                    NotifyPropertyChanged("WakkaB");
                }
            }
        }

        public BlitzPlayer DattoB
        {
            get { return dattoB; }
            set
            {
                if (DattoB != value)
                {
                    dattoB = value;
                    NotifyPropertyChanged("DattoB");
                }
            }
        }

        public BlitzPlayer LettyB
        {
            get { return lettyB; }
            set
            {
                if (LettyB != value)
                {
                    lettyB = value;
                    NotifyPropertyChanged("LettyB");
                }
            }
        }

        public BlitzPlayer JassuB
        {
            get { return jassuB; }
            set
            {
                if (JassuB != value)
                {
                    jassuB = value;
                    NotifyPropertyChanged("JassuB");
                }
            }
        }

        public BlitzPlayer BottaB
        {
            get { return bottaB; }
            set
            {
                if (BottaB != value)
                {
                    bottaB = value;
                    NotifyPropertyChanged("BottaB");
                }
            }
        }

        public BlitzPlayer KeepaB
        {
            get { return keepaB; }
            set
            {
                if (KeepaB != value)
                {
                    keepaB = value;
                    NotifyPropertyChanged("KeepaB");
                }
            }
        }

        public BlitzPlayer SelectedPlayer
        {
            get { return selectedPlayer; }
            set
            {
                if (SelectedPlayer != value)
                {
                    selectedPlayer = value;
                    NotifyPropertyChanged("SelectedPlayer");
                }
            }
        }

        public Character Auron
        {
            get { return auron; }
            set
            {
                if (Auron != value)
                {
                    auron = value;
                    NotifyPropertyChanged("Auron");
                }
            }
        }

        public Character Kimahri
        {
            get { return kimahri; }
            set
            {
                if (Kimahri != value)
                {
                    kimahri = value;
                    NotifyPropertyChanged("Kimahri");
                }
            }
        }

        public Character Lulu
        {
            get { return lulu; }
            set
            {
                if (Lulu != value)
                {
                    lulu = value;
                    NotifyPropertyChanged("Lulu");
                }
            }
        }

        public Character Rikku
        {
            get { return rikku; }
            set
            {
                if (Rikku != value)
                {
                    rikku = value;
                    NotifyPropertyChanged("Rikku");
                }
            }
        }

        public Character Tidus
        {
            get { return tidus; }
            set
            {
                if (Tidus != value)
                {
                    tidus = value;
                    NotifyPropertyChanged("Tidus");
                }
            }
        }

        public Character Wakka
        {
            get { return wakka; }
            set
            {
                if (Wakka != value)
                {
                    wakka = value;
                    NotifyPropertyChanged("Wakka");
                }
            }
        }

        public Character Yuna
        {
            get { return yuna; }
            set
            {
                if (Yuna != value)
                {
                    yuna = value;
                    NotifyPropertyChanged("Yuna");
                }
            }
        }

        public Character SelectedCharacter
        {
            get { return selectedCharacter; }
            set
            {
                if (SelectedCharacter != value)
                {
                    selectedCharacter = value;
                    NotifyPropertyChanged("SelectedCharacter");
                }
            }
        }

        public Aeon Valefor
        {
            get { return valefor; }
            set
            {
                if (Valefor == value) return;
                valefor = value;
                NotifyPropertyChanged("Valefor");
            }
        }

        public Aeon Ifrit 
        {
            get { return ifrit; }
            set 
            {
                if (Ifrit == value) return;
                ifrit = value;
                NotifyPropertyChanged("Ifrit");
            }
        }

        public Aeon Ixion
        {
            get { return ixion; }
            set
            {
                if (Ixion == value) return;
                ixion = value;
                NotifyPropertyChanged("Ixion");
            }
        }

        public Aeon Shiva
        {
            get { return shiva; }
            set
            {
                if (Shiva == value) return;
                shiva = value;
                NotifyPropertyChanged("Shiva");
            }
        }

        public Aeon Bahamut
        {
            get { return bahamut; }
            set
            {
                if (Bahamut == value) return;
                bahamut = value;
                NotifyPropertyChanged("Bahamut");
            }
        }

        public Aeon Anima
        {
            get { return anima; }
            set
            {
                if (Anima == value) return;
                anima = value;
                NotifyPropertyChanged("Anima");
            }
        }

        public Aeon Yojimbo
        {
            get { return yojimbo; }
            set
            {
                if (Yojimbo == value) return;
                yojimbo = value;
                NotifyPropertyChanged("Yojimbo");
            }
        }

        public Aeon Cindy
        {
            get { return cindy; }
            set
            {
                if (Cindy == value) return;
                cindy = value;
                NotifyPropertyChanged("Cindy");
            }
        }

        public Aeon Sandy
        {
            get { return sandy; }
            set
            {
                if (Sandy == value) return;
                sandy = value;
                NotifyPropertyChanged("Sandy");
            }
        }

        public Aeon Mindy
        {
            get { return mindy; }
            set
            {
                if (Mindy == value) return;
                mindy = value;
                NotifyPropertyChanged("Mindy");
            }
        }

        public Aeon SelectedAeon
        {
            get { return selectedAeon; }
            set
            {
                if (SelectedAeon == value) return;
                selectedAeon = value;
                NotifyPropertyChanged("SelectedAeon");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
