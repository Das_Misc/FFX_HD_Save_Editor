﻿using System;

namespace FFX_HD_Save_Editor.Models
{
    class CRC16_CCITT
    {
        public static byte[] CRC16CCITT(byte[] ba)
        {
            uint polynomial = 0x1021;
            int seed = 0xFFFF;
            int hash = 0;

            var table = new UInt16[256];
            for (UInt32 Index = 0, Entry, Mask; Index < table.Length; Index++)
            {
                Mask = Index << 8;
                Entry = 0;

                for (UInt32 Bit = 0; Bit < 8; Bit++)
                {
                    if (((Entry ^ Mask) & 0x8000) != 0)
                        Entry = (Entry << 1) ^ polynomial;
                    else
                        Entry = (Entry << 1);

                    Mask <<= 1;
                }

                table[Index] = (UInt16)Entry;
            }
            table[255] = 0;

            hash ^= seed;

            int start = 0;
            while (start < ba.Length)
                hash = (table[(hash >> 8) ^ ba[start++]] ^ (hash << 8)) & seed;

            hash ^= seed;

            byte[] hashValue = new[]
            {
                (byte)((hash >> 8) & 0xFF),
                (byte)((hash >> 0) & 0xFF)
            };

            Array.Reverse(hashValue);

            return hashValue;
        }
    }
}
