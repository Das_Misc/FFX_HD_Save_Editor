﻿using System.ComponentModel;

namespace FFX_HD_Save_Editor.Models
{
    public class Overdrive : INotifyPropertyChanged
    {
        private bool bit;
        private string name;

        public bool Bit
        {
            get { return bit; }
            set
            {
                if (Bit == value) return;
                bit = value;
                NotifyPropertyChanged("Bit");
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                if (Name == value) return;
                name = value;
                NotifyPropertyChanged("Name");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
