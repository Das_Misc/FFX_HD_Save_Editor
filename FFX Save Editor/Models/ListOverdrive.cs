﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFX_HD_Save_Editor.Models
{
    public class ListOverdrive : List<string>
    {
        public ListOverdrive()
        {
            Add("Spiral cut");
            Add("Slice & Dice");
            Add("Energy Rain");
            Add("Blitz Ace");
            Add("Dragon Fang");
            Add("Shooting Star");
            Add("Banishing Blade");
            Add("Tornado");
            Add("Jump");
            Add("Fire Breath");
            Add("Seed Cannon");
            Add("Self-Destruct");
            Add("Thrust Kick");
            Add("Stone Breath");
            Add("Aqua Breath");
            Add("Doom");
            Add("White Wind");
            Add("Bad Breath");
            Add("Mighty Guard");
            Add("Nova");
            Add("Element Reels");
            Add("Attack Reels");
            Add("Status Reels");
            Add("Aurochs Reels");
        }
    }
}
