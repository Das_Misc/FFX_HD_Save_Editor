﻿using System.ComponentModel;

namespace FFX_HD_Save_Editor.Models
{
    public class Character : Aeon
    {
        private bool aeonsOnlyOD;

        private byte percentDamageFromPoison, currentSphereLevel, totalSphereLevel;

        private int ap, affectionToTidus, enemiesDefeated, learnedODModes;

        private short tillWarriorOD, tillComradeOD, tillHealerOD, tillTacticianOD, tillVictimOD, tillDancerOD,
                      tillAvengerOD, tillSlayerOD, tillHeroOD, tillRookOD, tillVictorOD, tillCowardOD,
                      tillAllyOD, tillSuffererOD, tillDaredevilOD, tillLonerOD;

        public int AP
        {
            get { return ap; }
            set
            {
                if (ap == value) return;
                ap = value;
                NotifyPropertyChanged("AP");
            }
        }

        public int AffectionToTidus
        {
            get { return affectionToTidus; }
            set
            {
                if (affectionToTidus == value) return;
                affectionToTidus = value;
                NotifyPropertyChanged("AffectionToTidus");
            }
        }

        public byte PercentDamageFromPoison 
        {
            get { return percentDamageFromPoison; }
            set
            {
                if (percentDamageFromPoison == value) return;
                percentDamageFromPoison = value;
                NotifyPropertyChanged("PercentDamageFromPoison");
            }
        }

        public byte CurrentSphereLevel
        {
            get { return currentSphereLevel; }
            set
            {
                if (currentSphereLevel == value) return;
                currentSphereLevel = value;
                NotifyPropertyChanged("CurrentSphereLevel");
            }
        }

        public byte TotalSphereLevel
        {
            get { return totalSphereLevel; }
            set
            {
                if (totalSphereLevel == value) return;
                totalSphereLevel = value;
                NotifyPropertyChanged("TotalSphereLevel");
            }
        }

        public int EnemiesDefeated
        {
            get { return enemiesDefeated; }
            set
            {
                if (enemiesDefeated == value) return;
                enemiesDefeated = value;
                NotifyPropertyChanged("EnemiesDefeated");
            }
        }

        public bool AeonsOnlyOD
        {
            get { return aeonsOnlyOD; }
            set
            {
                if (aeonsOnlyOD == value) return;
                aeonsOnlyOD = value;
                NotifyPropertyChanged("AeonsOnlyOD");
            }
        }

        public short TillWarriorOD
        {
            get { return tillWarriorOD; }
            set
            {
                if (tillWarriorOD == value) return;
                tillWarriorOD = value;
                NotifyPropertyChanged("TillWarriorOD");
            }
        }

        public short TillComradeOD
        {
            get { return tillComradeOD; }
            set
            {
                if (tillComradeOD == value) return;
                tillComradeOD = value;
                NotifyPropertyChanged("TillComradeOD");
            }
        }

        public short TillHealerOD
        {
            get { return tillHealerOD; }
            set
            {
                if (tillHealerOD == value) return;
                tillHealerOD = value;
                NotifyPropertyChanged("TillHealerOD");
            }
        }

        public short TillTacticianOD
        {
            get { return tillTacticianOD; }
            set
            {
                if (tillTacticianOD == value) return;
                tillTacticianOD = value;
                NotifyPropertyChanged("TillTacticianOD");
            }
        }

        public short TillVictimOD
        {
            get { return tillVictimOD; }
            set
            {
                if (tillVictimOD == value) return;
                tillVictimOD = value;
                NotifyPropertyChanged("TillVictimOD");
            }
        }

        public short TillDancerOD
        {
            get { return tillDancerOD; }
            set
            {
                if (tillDancerOD == value) return;
                tillDancerOD = value;
                NotifyPropertyChanged("TillDancerOD");
            }
        }

        public short TillAvengerOD
        {
            get { return tillAvengerOD; }
            set
            {
                if (tillAvengerOD == value) return;
                tillAvengerOD = value;
                NotifyPropertyChanged("TillAvengerOD");
            }
        }

        public short TillSlayerOD
        {
            get { return tillSlayerOD; }
            set
            {
                if (tillSlayerOD == value) return;
                tillSlayerOD = value;
                NotifyPropertyChanged("TillSlayerOD");
            }
        }

        public short TillHeroOD
        {
            get { return tillHeroOD; }
            set
            {
                if (tillHeroOD == value) return;
                tillHeroOD = value;
                NotifyPropertyChanged("TillHeroOD");
            }
        }

        public short TillRookOD
        {
            get { return tillRookOD; }
            set
            {
                if (tillRookOD == value) return;
                tillRookOD = value;
                NotifyPropertyChanged("TillRookOD");
            }
        }

        public short TillVictorOD
        {
            get { return tillVictorOD; }
            set
            {
                if (tillVictorOD == value) return;
                tillVictorOD = value;
                NotifyPropertyChanged("TillVictorOD");
            }
        }

        public short TillCowardOD
        {
            get { return tillCowardOD; }
            set
            {
                if (tillCowardOD == value) return;
                tillCowardOD = value;
                NotifyPropertyChanged("TillCowardOD");
            }
        }

        public short TillAllyOD
        {
            get { return tillAllyOD; }
            set
            {
                if (tillAllyOD == value) return;
                tillAllyOD = value;
                NotifyPropertyChanged("TillAllyOD");
            }
        }

        public short TillSuffererOD
        {
            get { return tillSuffererOD; }
            set
            {
                if (tillSuffererOD == value) return;
                tillSuffererOD = value;
                NotifyPropertyChanged("TillSuffererOD");
            }
        }

        public short TillDaredevilOD
        {
            get { return tillDaredevilOD; }
            set
            {
                if (tillDaredevilOD == value) return;
                tillDaredevilOD = value;
                NotifyPropertyChanged("TillDaredevilOD");
            }
        }

        public short TillLonerOD
        {
            get { return tillLonerOD; }
            set
            {
                if (tillLonerOD == value) return;
                tillLonerOD = value;
                NotifyPropertyChanged("TillLonerOD");
            }
        }

        public int LearnedODModes
        {
            get { return learnedODModes; }
            set
            {
                if (learnedODModes == value) return;
                learnedODModes = value;
                NotifyPropertyChanged("LearnedODModes");
            }
        }
    }
}
