﻿using System.Collections.Generic;

namespace FFX_HD_Save_Editor.Models
{
    public class ListKeyItem : List<string>
    {
        public ListKeyItem()
        {
            Add("Cloudy Mirror");
            Add("Celestial Mirror");
            Add("Withered Bouquet");
            Add("Flint");
            Add("Summoner's Soul");
            Add("Aeon's Soul");
            Add("Jecht's Sphere");
            Add("Rusty Sword");
            Add("Sun Crest");
            Add("Sun Sigil");
            Add("Moon Crest");
            Add("Moon Sigil");
            Add("Mars Crest");
            Add("Mars Sigil");
            Add("Jupiter Crest");
            Add("Jupiter Sigil");
            Add("Saturn Crest");
            Add("Saturn Sigil");
            Add("Venus Crest");
            Add("Venus Sigil");
            Add("Mercury Crest");
            Add("Mercury Sigil");
            Add("Al Bhed Primer I");
            Add("Al Bhed Primer II");
            Add("Al Bhed Primer III");
            Add("Al Bhed Primer IV");
            Add("Al Bhed Primer V");
            Add("Al Bhed Primer VI");
            Add("Al Bhed Primer VII");
            Add("Al Bhed Primer VIII");
            Add("Al Bhed Primer IX");
            Add("Al Bhed Primer X");
            Add("Al Bhed Primer XI");
            Add("Al Bhed Primer XII");
            Add("Al Bhed Primer XIII");
            Add("Al Bhed Primer XIV");
            Add("Al Bhed Primer XV");
            Add("Al Bhed Primer XVI");
            Add("Al Bhed Primer XVII");
            Add("Al Bhed Primer XVIII");
            Add("Al Bhed Primer XIX");
            Add("Al Bhed Primer XX");
            Add("Al Bhed Primer XXI");
            Add("Al Bhed Primer XXII");
            Add("Al Bhed Primer XXIII");
            Add("Al Bhed Primer XXIV");
            Add("Al Bhed Primer XXV");
            Add("Al Bhed Primer XXVI");
            Add("Blossom Crown");
            Add("Flower Scepter");
            Add("Mark of Conquest");
        }

        public void InternalOrder()
        {
            Clear();

            Add("Withered Bouquet");
            Add("Flint");
            Add("Cloudy Mirror");
            Add("Celestial Mirror");

            Add("Al Bhed Primer I");
            Add("Al Bhed Primer II");
            Add("Al Bhed Primer III");
            Add("Al Bhed Primer IV");

            Add("Al Bhed Primer V");
            Add("Al Bhed Primer VI");
            Add("Al Bhed Primer VII");
            Add("Al Bhed Primer VIII");

            Add("Al Bhed Primer IX");
            Add("Al Bhed Primer X");
            Add("Al Bhed Primer XI");
            Add("Al Bhed Primer XII");

            Add("Al Bhed Primer XIII");
            Add("Al Bhed Primer XIV");
            Add("Al Bhed Primer XV");
            Add("Al Bhed Primer XVI");

            Add("Al Bhed Primer XVII");
            Add("Al Bhed Primer XVIII");
            Add("Al Bhed Primer XIX");
            Add("Al Bhed Primer XX");

            Add("Al Bhed Primer XXI");
            Add("Al Bhed Primer XXII");
            Add("Al Bhed Primer XXIII");
            Add("Al Bhed Primer XXIV");

            Add("Al Bhed Primer XXV");
            Add("Al Bhed Primer XXVI");
            Add("Summoner's Soul");
            Add("Aeon's Soul");

            Add("Jecht's Sphere");
            Add("Rusty Sword");
            Add("UNUSED");
            Add("Sun Crest");

            Add("Sun Sigil");
            Add("Moon Crest");
            Add("Moon Sigil");
            Add("Mars Crest");

            Add("Mars Sigil");
            Add("Mark of Conquest");
            Add("Saturn Crest");
            Add("Saturn Sigil");

            Add("Jupiter Crest");
            Add("Jupiter Sigil");
            Add("Venus Crest");
            Add("Venus Sigil");

            Add("Mercury Crest");
            Add("Mercury Sigil");
            Add("Blossom Crown");
            Add("Flower Scepter");
        }
    }
}
