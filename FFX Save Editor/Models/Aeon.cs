﻿using System.ComponentModel;

namespace FFX_HD_Save_Editor.Models
{
    public class Aeon : INotifyPropertyChanged
    {
        private int addedHP, addedMP;
        private byte addedStrength, addedDefense, addedMagic, addedMagicDefense, addedAgility, addedLuck, addedEvasion, addedAccuracy;
        private int baseHP, baseMP;
        private byte baseStrength, baseDefense, baseMagic, baseMagicDefense, baseAgility, baseLuck, baseEvasion, baseAccuracy;
        private byte currentOverdriveGauge, maxOverdriveGauge;

        public byte AddedAccuracy
        {
            get { return addedAccuracy; }
            set
            {
                if (addedAccuracy == value) return;
                addedAccuracy = value;
                NotifyPropertyChanged("AddedAccuracy");
            }
        }

        public byte AddedAgility
        {
            get { return addedAgility; }
            set
            {
                if (addedAgility == value) return;
                addedAgility = value;
                NotifyPropertyChanged("AddedAgility");
            }
        }

        public byte AddedDefense
        {
            get { return addedDefense; }
            set
            {
                if (addedDefense == value) return;
                addedDefense = value;
                NotifyPropertyChanged("AddedDefense");
            }
        }

        public byte AddedEvasion
        {
            get { return addedEvasion; }
            set
            {
                if (addedEvasion == value) return;
                addedEvasion = value;
                NotifyPropertyChanged("AddedEvasion");
            }
        }

        public int AddedHP
        {
            get { return addedHP; }
            set
            {
                if (addedHP == value) return;
                addedHP = value;
                NotifyPropertyChanged("AddedHP");
            }
        }

        public byte AddedLuck
        {
            get { return addedLuck; }
            set
            {
                if (addedLuck == value) return;
                addedLuck = value;
                NotifyPropertyChanged("AddedLuck");
            }
        }

        public byte AddedMagic
        {
            get { return addedMagic; }
            set
            {
                if (addedMagic == value) return;
                addedMagic = value;
                NotifyPropertyChanged("AddedMagic");
            }
        }

        public byte AddedMagicDefense
        {
            get { return addedMagicDefense; }
            set
            {
                if (addedMagicDefense == value) return;
                addedMagicDefense = value;
                NotifyPropertyChanged("AddedMagicDefense");
            }
        }

        public int AddedMP
        {
            get { return addedMP; }
            set
            {
                if (addedMP == value) return;
                addedMP = value;
                NotifyPropertyChanged("AddedMP");
            }
        }

        public byte AddedStrength
        {
            get { return addedStrength; }
            set
            {
                if (addedStrength == value) return;
                addedStrength = value;
                NotifyPropertyChanged("AddedStrength");
            }
        }

        public byte BaseAccuracy
        {
            get { return baseAccuracy; }
            set
            {
                if (baseAccuracy == value) return;
                baseAccuracy = value;
                NotifyPropertyChanged("BaseAccuracy");
                NotifyPropertyChanged("TotalAccuracy");
            }
        }

        public byte BaseAgility
        {
            get { return baseAgility; }
            set
            {
                if (baseAgility == value) return;
                baseAgility = value;
                NotifyPropertyChanged("BaseAgility");
                NotifyPropertyChanged("TotalAgility");
            }
        }

        public byte BaseDefense
        {
            get { return baseDefense; }
            set
            {
                if (baseDefense == value) return;
                baseDefense = value;
                NotifyPropertyChanged("BaseDefense");
                NotifyPropertyChanged("TotalDefense");
            }
        }

        public byte BaseEvasion
        {
            get { return baseEvasion; }
            set
            {
                if (baseEvasion == value) return;
                baseEvasion = value;
                NotifyPropertyChanged("BaseEvasion");
                NotifyPropertyChanged("TotalEvasion");
            }
        }

        public int BaseHP
        {
            get { return baseHP; }
            set
            {
                if (baseHP == value) return;
                baseHP = value;
                NotifyPropertyChanged("BaseHP");
                NotifyPropertyChanged("TotalHP");
            }
        }

        public byte BaseLuck
        {
            get { return baseLuck; }
            set
            {
                if (baseLuck == value) return;
                baseLuck = value;
                NotifyPropertyChanged("BaseLuck");
                NotifyPropertyChanged("TotalLuck");
            }
        }

        public byte BaseMagic
        {
            get { return baseMagic; }
            set
            {
                if (baseMagic == value) return;
                baseMagic = value;
                NotifyPropertyChanged("BaseMagic");
                NotifyPropertyChanged("TotalMagic");
            }
        }

        public byte BaseMagicDefense
        {
            get { return baseMagicDefense; }
            set
            {
                if (baseMagicDefense == value) return;
                baseMagicDefense = value;
                NotifyPropertyChanged("BaseMagicDefense");
                NotifyPropertyChanged("TotalMagicDefense");
            }
        }

        public int BaseMP
        {
            get { return baseMP; }
            set
            {
                if (baseMP == value) return;
                baseMP = value;
                NotifyPropertyChanged("BaseMP");
                NotifyPropertyChanged("TotalMP");
            }
        }

        public byte BaseStrength
        {
            get { return baseStrength; }
            set
            {
                if (baseStrength == value) return;
                baseStrength = value;
                NotifyPropertyChanged("BaseStrength");
                NotifyPropertyChanged("TotalStrength");
            }
        }

        public byte CurrentOverdriveGauge
        {
            get { return currentOverdriveGauge; }
            set
            {
                if (currentOverdriveGauge == value) return;
                currentOverdriveGauge = value;
                NotifyPropertyChanged("CurrentOverdriveGauge");
            }
        }

        public byte MaxOverdriveGauge
        {
            get { return maxOverdriveGauge; }
            set
            {
                if (maxOverdriveGauge == value) return;
                maxOverdriveGauge = value;
                NotifyPropertyChanged("MaxOverdriveGauge");
            }
        }

        public byte TotalAccuracy
        {
            get { return addedAccuracy + baseAccuracy <= 255 ? (byte)(addedAccuracy + baseAccuracy) : (byte)255; }
        }

        public byte TotalAgility
        {
            get { return addedAgility + baseAgility <= 255 ? (byte)(addedAgility + baseAgility) : (byte)255; }
        }

        public byte TotalDefense
        {
            get { return addedDefense + baseDefense <= 255 ? (byte)(addedDefense + baseDefense) : (byte)255; }
        }

        public byte TotalEvasion
        {
            get { return addedEvasion + baseEvasion <= 255 ? (byte)(addedEvasion + baseEvasion) : (byte)255; }
        }

        public int TotalHP
        {
            get { return addedHP + baseHP <= 99999 ? addedHP + baseHP : 99999; }
        }

        public byte TotalLuck
        {
            get { return addedLuck + baseLuck <= 255 ? (byte)(addedLuck + baseLuck) : (byte)255; }
        }

        public byte TotalMagic
        {
            get { return addedMagic + baseMagic <= 255 ? (byte)(addedMagic + baseMagic) : (byte)255; }
        }

        public byte TotalMagicDefense
        {
            get { return addedMagicDefense + baseMagicDefense <= 255 ? (byte)(addedMagicDefense + baseMagicDefense) : (byte)255; }
        }

        public int TotalMP
        {
            get { return addedMP + baseMP <= 9999 ? addedMP + baseMP : 9999; }
        }

        public byte TotalStrength
        {
            get { return addedStrength + baseStrength <= 255 ? (byte)(addedStrength + baseStrength) : (byte)255; }
        }

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion INotifyPropertyChanged implementation
    }
}
