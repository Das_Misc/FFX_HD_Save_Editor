﻿using System.Collections.Generic;

namespace FFX_HD_Save_Editor.Models
{
    public class ListCactuarStones : List<string>
    {
        public ListCactuarStones()
        {
            Add("Not started");
            Add("#1: Tomay");
            Add("#1: Tomay (Complete)");
            Add("#2: Rovivea");
            Add("#2: Rovivea (Complete)");
            Add("#3: Chava");
            Add("#3: Chava (Complete)");
            Add("#4: Alek & Aloja");
            Add("#4: Alek & Aloja (Complete)");
            Add("#5: Vachella");
            Add("#5: Vachella (Complete)");
            Add("#6: Robeya");
            Add("#6: Robeya (Complete)");
            Add("#7: Isrra");
            Add("#7: Isrra (Complete)");
            Add("#8: Elio");
            Add("#8: Elio (Fahrenheit)");
            Add("#8: Elio (Complete)");
            Add("#9: Flaile (Complete)");
            Add("Unlocked Cactuar Village");
        }
    }
}
