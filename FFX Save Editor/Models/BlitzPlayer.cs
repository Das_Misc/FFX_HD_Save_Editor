﻿using System.ComponentModel;

namespace FFX_HD_Save_Editor.Models
{
    public class BlitzPlayer : INotifyPropertyChanged
    {
        private byte contractGamesLeft, level, techniqueSlots;
        private ushort experience;
        private uint defensiveTechniques, learnedTechniques, offensiveTechniques;

        public byte ContractGamesLeft
        {
            get { return contractGamesLeft; }
            set
            {
                if (ContractGamesLeft != value)
                {
                    contractGamesLeft = value;
                    NotifyPropertyChanged("ContractGamesLeft");
                }
            }
        }

        public byte Level
        {
            get { return level; }
            set
            {
                if (Level != value)
                {
                    level = value;
                    NotifyPropertyChanged("Level");
                }
            }
        }

        public byte TechniqueSlots
        {
            get { return techniqueSlots; }
            set
            {
                if (TechniqueSlots != value)
                {
                    techniqueSlots = value;
                    NotifyPropertyChanged("TechniqueSlots");
                }
            }
        }

        public ushort Experience
        {
            get { return experience; }
            set
            {
                if (Experience != value)
                {
                    experience = value;
                    NotifyPropertyChanged("Experience");
                }
            }
        }

        public uint DefensiveTechniques
        {
            get { return defensiveTechniques; }
            set
            {
                if (DefensiveTechniques != value)
                {
                    defensiveTechniques = value;
                    NotifyPropertyChanged("DefensiveTechniques");
                }
            }
        }

        public uint OffensiveTechniques
        {
            get { return offensiveTechniques; }
            set
            {
                if (OffensiveTechniques != value)
                {
                    offensiveTechniques = value;
                    NotifyPropertyChanged("OffensiveTechniques");
                }
            }
        }

        public uint LearnedTechniques
        {
            get { return learnedTechniques; }
            set
            {
                if (LearnedTechniques != value)
                {
                    learnedTechniques = value;
                    NotifyPropertyChanged("LearnedTechniques");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
