﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;

namespace Saves_Reencryptor
{
    class PFD_Header
    {
        #region Constructor

        /// <summary>
        /// Given a PS3 PARAM.PFD file or a PS3 save game folder, decrypts the contents of the PARAM.PFD file and returns a PFD Header object.
        /// </summary>
        /// <param name="paramOrFolder"></param>
        internal PFD_Header(string paramOrFolder, byte[] secureFileID)
        {
            if (File.Exists(paramOrFolder))
            {
                paramPFD = paramOrFolder;

                saveFolder = Path.GetDirectoryName(paramOrFolder);
            }
            else if (Directory.Exists(paramOrFolder))
            {
                paramPFD = Path.Combine(paramOrFolder, "param.pfd");

                saveFolder = paramOrFolder;
            }

            if (!File.Exists(paramPFD))
                return;

            this.secureFileID = new byte[0x10];
            Buffer.BlockCopy(secureFileID, 0, this.secureFileID, 0, this.secureFileID.Length);

            byte[] contents = File.ReadAllBytes(paramPFD);

            using (MemoryStream ms = new MemoryStream(contents))
            using (BinaryReader br = new BinaryReader(ms))
            {
                SetIsMagicValid(br.ReadBytes(8));

                if (!IsMagicValid)
                    return;

                fileSize = contents.Length;

                version = br.ReadInt64();

                PFDHeaderTableIV = br.ReadBytes(0x10);

                PFDHeaderTable = br.ReadBytes(0x40);

                decryptedPFDHeaderTable = Cryptographic_Engines.Decrypt(PFDHeaderTable, Syscon_iso_2, PFDHeaderTableIV, CipherMode.CBC, PaddingMode.Zeros);
                fileHMACKey = DecryptedPFDHeaderTable;

                using (MemoryStream msDecryptedPFDHeaderTable = new MemoryStream(DecryptedPFDHeaderTable))
                using (BinaryReader brDecryptedPFDHeaderTable = new BinaryReader(msDecryptedPFDHeaderTable))
                {
                    hash1 = brDecryptedPFDHeaderTable.ReadBytes(0x14);
                    hash2 = brDecryptedPFDHeaderTable.ReadBytes(0x14);
                    hashKey = brDecryptedPFDHeaderTable.ReadBytes(0x14);
                    padding = brDecryptedPFDHeaderTable.ReadBytes(4);
                }

                if (Version == 0x400000000000000)
                    key = new HMACSHA1(KeygenV4).ComputeHash(hashKey, 0, hashKey.Length);
                else
                    key = hashKey;

                SetXYReservedEntries(br.ReadBytes(8));
                SetTotalReservedEntries(br.ReadBytes(8));
                SetUsedEntries(br.ReadBytes(8));

                PopulateXYEntries(br);

                PopulateEntries(br);

                SetHashData();
            }
        }

        #endregion

        #region Fields

        private PFD_XY_Entry[] xyEntries;
        private PFD_Entry[] entries;

        private bool isMagicValid;

        private long version, xyReservedEntries, totalReservedEntries, usedEntries;

        private byte[] pfdHeaderTableIV, pfdHeaderTable;
        private byte[] decryptedPFDHeaderTable, fileHMACKey;
        private byte[] hash1, hash2, hashKey, padding, key, hashData;
        private byte[] secureFileID;

        private string paramPFD, saveFolder;

        private int fileSize;

        #endregion

        #region Properties

        internal PFD_XY_Entry[] XYEntries { get { return xyEntries; } }
        internal PFD_Entry[] Entries { get { return entries; } }

        internal static readonly byte[] KeygenV4 = { 0x6B, 0x1A, 0xCE, 0xA2, 0x46, 0xB7, 0x45, 0xFD, 0x8F, 0x93, 0x76, 0x3B, 0x92, 0x05, 0x94, 0xCD, 0x53, 0x48, 0x3B, 0x82 };

        internal static readonly byte[] Magic = { 0x00, 0x00, 0x00, 0x00, 0x50, 0x46, 0x44, 0x42 };

        internal static readonly byte[] Syscon_iso_2 = { 0xD4, 0x13, 0xB8, 0x96, 0x63, 0xE1, 0xFE, 0x9F, 0x75, 0x14, 0x3D, 0x3B, 0xB4, 0x56, 0x52, 0x74 };

        internal bool IsMagicValid { get { return isMagicValid; } }

        internal long Version { get { return version; } }
        internal long XYReservedEntries { get { return xyReservedEntries; } }
        internal long TotalReservedEntries { get { return totalReservedEntries; } }
        internal long UsedEntries { get { return usedEntries; } }

        internal byte[] PFDHeaderTableIV
        {
            get { return pfdHeaderTableIV; }
            set { pfdHeaderTableIV = new byte[0x10]; Buffer.BlockCopy(value, 0, pfdHeaderTableIV, 0, pfdHeaderTableIV.Length); } 
        }
        internal byte[] PFDHeaderTable
        {
            get { return pfdHeaderTable; }
            set { pfdHeaderTable = new byte[0x40]; Buffer.BlockCopy(value, 0, pfdHeaderTable, 0, pfdHeaderTable.Length); }
        }
        internal byte[] DecryptedPFDHeaderTable { get { return decryptedPFDHeaderTable; } }
        internal byte[] FileHMACKey
        {
            get { return fileHMACKey; }
            set { fileHMACKey = new byte[0x14]; Buffer.BlockCopy(value, 0x28, fileHMACKey, 0, fileHMACKey.Length); }
        }

        internal byte[] Hash1
        { 
            get { return hash1; }
            set
            {
                hash1 = new byte[0x14];

                Buffer.BlockCopy(value, 0, hash1, 0, hash1.Length);
            }
        }

        internal byte[] Hash2
        {
            get { return hash2; }
            set
            {
                hash2 = new byte[0x14];

                Buffer.BlockCopy(value, 0, hash2, 0, hash2.Length);
            }
        }

        internal byte[] HashKey { get { return hashKey; } }

        internal byte[] Padding { get { return padding;} }

        internal byte[] Key { get { return key; } }

        internal byte[] HashData { get { return hashData; } }

        internal string ParamPFD { get { return paramPFD; } }
        internal string SaveFolder { get { return saveFolder; } set { saveFolder = value; } }

        internal int FileSize { get { return fileSize; } }

        #endregion

        #region Private Methods

        private void CheckEndianness(ref byte[] value)
        {
            if (BitConverter.IsLittleEndian)
                Array.Reverse(value);
        }

        private long CheckEndianness(long value)
        {
            byte[] tmp = BitConverter.GetBytes(value);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(tmp);

            return BitConverter.ToInt64(tmp, 0);
        }

        private void SetIsMagicValid(byte[] value)
        {
            isMagicValid = value.Length == Magic.Length && value.SequenceEqual(Magic) ? true : false;
        }

        private void PopulateXYEntries(BinaryReader br)
        {
            br.BaseStream.Position = 0x78;

            xyEntries = new PFD_XY_Entry[XYReservedEntries];
            for (int i = 0; i < XYReservedEntries; i++) // Initialize entries and set their virtual index IDs
            {
                xyEntries[i] = new PFD_XY_Entry();

                xyEntries[i].SetVirtualIndexID(br.ReadBytes(8));
            }

            br.BaseStream.Position = 0x7B60;

            for (int i = 0; i < XYReservedEntries; i++)
                xyEntries[i].HMACHash = br.ReadBytes(0x14);
        }

        private void PopulateEntries(BinaryReader br)
        {
            br.BaseStream.Position = 0x240;

            entries = new PFD_Entry[UsedEntries];
            for (int i = 0; i < UsedEntries; i++) // Fill entries' information
            {
                entries[i] = new PFD_Entry();

                entries[i].SetVirtualIndexID(br.ReadBytes(8));
                entries[i].SetFilename(br.ReadBytes(0x41));
                entries[i].RandomGarbage = br.ReadBytes(7);
                entries[i].Key = br.ReadBytes(0x40);
                entries[i].HMACHash1 = br.ReadBytes(0x14);
                entries[i].HMACHash2 = br.ReadBytes(0x14);
                entries[i].HMACHash3 = br.ReadBytes(0x14);
                entries[i].HMACHash4 = br.ReadBytes(0x14);
                entries[i].SetHMACHashes();
                entries[i].Padding = br.ReadBytes(0x28);
                entries[i].SetFilesize(br.ReadBytes(8));

                entries[i].SetHashKey(i, secureFileID);

                if (entries[i].Filename.ToLower() != "param.sfo")
                    entries[i].SetDecryptionKey(Syscon_iso_2);
            }
        }

        private void SetXYReservedEntries(byte[] value)
        {
            CheckEndianness(ref value);

            xyReservedEntries = BitConverter.ToInt64(value, 0);
        }

        private void SetTotalReservedEntries(byte[] value)
        {
            CheckEndianness(ref value);

            totalReservedEntries = BitConverter.ToInt64(value, 0);
        }

        private void SetUsedEntries(byte[] value)
        {
            CheckEndianness(ref value);

            usedEntries = BitConverter.ToInt64(value, 0);
        }

        private void SetHashData()
        {
            using (MemoryStream ms = new MemoryStream())
            using (BinaryWriter bw = new BinaryWriter(ms))
            {
                bw.Write(CheckEndianness(XYReservedEntries));
                bw.Write(CheckEndianness(TotalReservedEntries));
                bw.Write(CheckEndianness(UsedEntries));

                foreach (var xyEntry in XYEntries)
                    bw.Write(CheckEndianness(xyEntry.VirtualIndexID));

                hashData = ms.ToArray();
            }
        }

        #endregion
    }
}
