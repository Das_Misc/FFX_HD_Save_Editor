﻿using System;
using System.IO;
using System.Linq;

namespace Saves_Reencryptor
{
    class SFO_Header
    {
        #region Constructor

        internal SFO_Header(string paramOrFolder)
        {
            if (File.Exists(paramOrFolder))
            {
                paramSFO = paramOrFolder;

                folder = Path.GetDirectoryName(paramOrFolder);
            }
            else if (Directory.Exists(paramOrFolder))
            {
                paramSFO = Path.Combine(paramOrFolder, "param.sfo");

                folder = paramOrFolder;
            }

            byte[] contents = File.ReadAllBytes(paramSFO);

            using (MemoryStream ms = new MemoryStream(contents))
            using (BinaryReader br = new BinaryReader(ms))
            {
                SetIsMagicValid(br.ReadBytes(4));

                if (!IsMagicValid)
                    return;

                fileSize = contents.Length;

                SetVersion(br.ReadBytes(4));
                SetKeyTableOffset(br.ReadBytes(4));
                SetDataTableOffset(br.ReadBytes(4));
                SetTablesEntriesCount(br.ReadBytes(4));

                PopulateEntries(br);
            }
        }

        #endregion

        #region Fields

        private SFO_Entry[] entries;

        private bool isMagicValid;

        private uint version, keyTableOffset, dataTableOffset, tablesEntriesCount;

        private string paramSFO, folder;

        private int fileSize;

        #endregion

        #region Properties

        internal SFO_Entry[] Entries { get { return entries; } }

        internal static readonly byte[] Magic = { 0x00, 0x50, 0x53, 0x46 };

        internal bool IsMagicValid { get { return isMagicValid; } }

        internal uint Version { get { return version; } }
        internal uint KeyTableOffset { get { return keyTableOffset; } }
        internal uint DataTableOffset { get { return dataTableOffset; } }
        internal uint TablesEntriesCount { get { return tablesEntriesCount; } }

        internal string ParamSFO { get { return paramSFO; } }
        internal string Folder { get { return folder; } set { folder = value; } }

        internal int FileSize { get { return fileSize; } }

        #endregion

        #region Private Methods

        private void PopulateEntries(BinaryReader br)
        {
            for (int i = 0; i < entries.Length; i++)
            {
                entries[i].SetKeyOffset(br.ReadBytes(2));
                entries[i].SetDataFormat(br.ReadBytes(2));
                entries[i].SetDataLength(br.ReadBytes(4));
                entries[i].SetDataMaxLength(br.ReadBytes(4));
                entries[i].SetDataOffset(br.ReadBytes(4));
            }
            
            for (int i = 0; i < entries.Length - 1; i++)
                entries[i].SetKeyName(br.ReadBytes(entries[i + 1].KeyOffset - entries[i].KeyOffset));

            entries[entries.Length - 1].SetKeyName(br.ReadBytes((int)(dataTableOffset - keyTableOffset - entries[entries.Length - 1].KeyOffset)));

            for (int i = 0; i < entries.Length; i++)
                entries[i].SetKeyValue(br.ReadBytes((int)entries[i].DataMaxLength), entries[i].DataFormat);
        }

        private void SetIsMagicValid(byte[] value)
        {
            isMagicValid = value.Length == Magic.Length && value.SequenceEqual(Magic) ? true : false;
        }

        #endregion

        #region internal Methods

        internal void SetVersion(byte[] value)
        {
            version = BitConverter.ToUInt32(value, 0);
        }

        internal void SetKeyTableOffset(byte[] value)
        {
            keyTableOffset = BitConverter.ToUInt32(value, 0);
        }

        internal void SetDataTableOffset(byte[] value)
        {
            dataTableOffset = BitConverter.ToUInt32(value, 0);
        }

        internal void SetTablesEntriesCount(byte[] value)
        {
            tablesEntriesCount = BitConverter.ToUInt32(value, 0);

            entries = new SFO_Entry[tablesEntriesCount];
            for (int i = 0; i < entries.Length; i++)
                entries[i] = new SFO_Entry();
        }

        #endregion
    }
}
