﻿using System;

namespace Saves_Reencryptor
{
    class PFD_XY_Entry
    {
        #region Fields

        private long virtualIndexID;
        private bool used;
        private byte[] hmacHash;

        #endregion

        #region Properties

        internal long VirtualIndexID { get { return virtualIndexID; } }

        internal bool Used { get { return used; } }

        internal byte[] HMACHash
        {
            get { return hmacHash; }
            set
            {
                hmacHash = new byte[0x14];

                Buffer.BlockCopy(value, 0, hmacHash, 0, hmacHash.Length);
            }
        }

        #endregion

        #region Private Methods

        private void CheckEndianness(ref byte[] value)
        {
            if (BitConverter.IsLittleEndian)
                Array.Reverse(value);
        }

        #endregion

        #region Internal Methods

        internal void SetVirtualIndexID(byte[] value)
        {
            CheckEndianness(ref value);

            virtualIndexID = BitConverter.ToInt64(value, 0);

            used = VirtualIndexID == 0x72 ? false : true;
        }

        #endregion
    }
}
