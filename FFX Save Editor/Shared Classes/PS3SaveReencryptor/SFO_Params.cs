﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace Saves_Reencryptor
{
    class SFO_Params
    {
        #region Fields

        byte[] rawContents;

        int userID;

        string psID, accountID;

        #endregion

        #region Properties

        internal byte[] RawContents { get { return rawContents; } }

        internal int UserID
        {
            get { return userID; }
            set
            {
                if (UserID == value) return;
                userID = CheckEndianness(value);
                UpdateRawContents();
            }
        }

        internal string PSID
        {
            get { return psID; }
            set
            {
                if (PSID == value) return;
                psID = value;
                UpdateRawContents();
            }
        }

        internal string AccountID
        {
            get { return accountID; }
            set
            {
                if (AccountID == value) return;
                accountID = value;
                UpdateRawContents();
            }
        }

        #endregion

        #region Constructor

        internal SFO_Params(byte[] @params)
        {
            rawContents = new byte[@params.Length];
            Buffer.BlockCopy(@params, 0, rawContents, 0, @params.Length);

            using (MemoryStream ms = new MemoryStream(@params))
            using (BinaryReader br = new BinaryReader(ms))
            {
                ms.Position = 0x18;

                SetUserID(br.ReadBytes(4));
                SetPSID(br.ReadBytes(0x10));

                ms.Position += 4;

                SetAccountID(br.ReadBytes(0x10));
            }
        }

        #endregion Constructor

        #region Private Methods

        private void CheckEndianness(ref byte[] value)
        {
            if (BitConverter.IsLittleEndian)
                Array.Reverse(value);
        }

        private int CheckEndianness(int value)
        {
            byte[] tmp = BitConverter.GetBytes(value);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(tmp);

            return BitConverter.ToInt32(tmp, 0);
        }

        private byte[] HexToBA(string value)
        {
            return Enumerable.Range(0, value.Length)
                     .Where(x => x % 2 == 0)
                     .Select(x => Convert.ToByte(value.Substring(x, 2), 16))
                     .ToArray();
        }

        private void SetUserID(byte[] value)
        {
            CheckEndianness(ref value);

            userID = BitConverter.ToInt32(value, 0);
        }

        private void SetPSID(byte[] value)
        {
            psID = BitConverter.ToString(value, 0).Replace("-", "").ToUpper();
        }

        private void SetAccountID(byte[] value)
        {
            accountID = Encoding.UTF8.GetString(value);
        }

        private void UpdateRawContents()
        {
            using (MemoryStream ms = new MemoryStream(rawContents))
            using (BinaryWriter bw = new BinaryWriter(ms))
            {
                ms.Position = 0x18;

                bw.Write(CheckEndianness(UserID));
                bw.Write(HexToBA(PSID));
                bw.Write(CheckEndianness(UserID));
                
                bw.Write(Encoding.UTF8.GetBytes(AccountID));
            }
        }

        #endregion
    }
}
