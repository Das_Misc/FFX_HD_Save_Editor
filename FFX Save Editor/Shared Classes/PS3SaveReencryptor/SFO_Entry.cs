﻿using System;
using System.Text;

namespace Saves_Reencryptor
{
    class SFO_Entry
    {
        #region Fields

        SFO_Params param;

        private ushort keyOffset, dataFormat; // dataFormat: 0x4 = UTF-8 not null terminated, 0x204 = UTF-8 null terminated, 0x404 = UInt32
        private uint dataLength, dataMaxLength, dataOffset;
        private string keyName, keyValue;

        #endregion

        #region Properties

        internal SFO_Params Param { get { return param; } }

        internal ushort KeyOffset { get { return keyOffset; } }
        internal ushort DataFormat { get { return dataFormat; } }

        internal uint DataLength { get { return dataLength; } }
        internal uint DataMaxLength { get { return dataMaxLength; } }
        internal uint DataOffset { get { return dataOffset; } }

        internal string KeyName { get { return keyName; } set { keyName = value; } }
        internal string KeyValue { get { return keyValue; } set { keyValue = value; } }

        #endregion
        
        #region internal Methods

        internal void SetKeyOffset(byte[] value)
        {
            keyOffset = BitConverter.ToUInt16(value, 0);
        }

        internal void SetDataFormat(byte[] value)
        {
            dataFormat = BitConverter.ToUInt16(value, 0);
        }

        internal void SetDataLength(byte[] value)
        {
            dataLength = BitConverter.ToUInt32(value, 0);
        }

        internal void SetDataMaxLength(byte[] value)
        {
            dataMaxLength = BitConverter.ToUInt32(value, 0);
        }

        internal void SetDataOffset(byte[] value)
        {
            dataOffset = BitConverter.ToUInt32(value, 0);
        }

        internal void SetKeyName(byte[] value)
        {
            keyName = Encoding.UTF8.GetString(value);

            keyName = keyName.Replace("\0", "");
        }

        internal void SetKeyValue(byte[] value, int dataFormat)
        {
            if (keyName == "PARAMS")
                param = new SFO_Params(value);

            if (dataFormat == 0x4 || dataFormat == 0x204)
                keyValue = Encoding.UTF8.GetString(value);

            if (dataFormat == 0x204)
                keyValue = keyValue.Replace("\0", "");

            if (dataFormat == 0x404)
                keyValue = BitConverter.ToUInt32(value, 0).ToString();
        }

        #endregion
    }
}
