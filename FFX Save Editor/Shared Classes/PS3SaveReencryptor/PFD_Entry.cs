﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Saves_Reencryptor
{
    class PFD_Entry
    {
        #region Fields

        private static readonly byte[] tropSysDatKey = { 0xB0, 0x80, 0xC4, 0x0F, 0xF3, 0x58, 0x64, 0x36, 0x89, 0x28, 0x17, 0x36, 0xA6, 0xBF, 0x15, 0x89, 0x2C, 0xFE, 0xA4, 0x36 };
        private static readonly byte[] tropUsrDatKey = { 0x87, 0x11, 0xEF, 0xF4, 0x06, 0x91, 0x3F, 0x09, 0x37, 0xF1, 0x15, 0xFA, 0xB2, 0x3D, 0xE1, 0xA9, 0x89, 0x7A, 0x78, 0x9A };
        private static readonly byte[] tropTrnsDatKey = { 0x91, 0xEE, 0x81, 0x55, 0x5A, 0xCC, 0x1C, 0x4F, 0xB5, 0xAA, 0xE5, 0x46, 0x2C, 0xFE, 0x1C, 0x62, 0xA4, 0xAF, 0x36, 0xA5 };
        private static readonly byte[] tropConfSfmKey = { 0xE2, 0xED, 0x33, 0xC7, 0x1C, 0x44, 0x4E, 0xEB, 0xC1, 0xE2, 0x3D, 0x63, 0x5A, 0xD8, 0xE8, 0x2F, 0x4E, 0xCA, 0x4E, 0x94 };
        private static readonly byte[] saveGameParamSfoKey = { 0x0C, 0x08, 0x00, 0x0E, 0x09, 0x05, 0x04, 0x04, 0x0D, 0x01, 0x0F, 0x00, 0x04, 0x06, 0x02, 0x02, 0x09, 0x06, 0x0D, 0x03 };
        private static readonly byte[] fallbackDiscHashKey = { 0xD1, 0xC1, 0xE1, 0x0B, 0x9C, 0x54, 0x7E, 0x68, 0x9B, 0x80, 0x5D, 0xCD, 0x97, 0x10, 0xCE, 0x8D };
        private static readonly byte[] authenticationID = { 0x10, 0x10, 0x00, 0x00, 0x01, 0x00, 0x00, 0x03 };

        private long virtualIndexID, filesize, alignedSize;
        private bool used;
        private string filename;
        private byte[] randomGarbage, key, hmacHash1, hmacHash2, hmacHash3, hmacHash4, padding;
        private byte[] hashKey, decryptionKey, consoleID, hashData;
        
        private List<byte[]> hmacHashes;

        #endregion

        #region Properties

        internal long VirtualIndexID { get { return virtualIndexID; } }
        internal long Filesize { get { return filesize; } }
        internal long AlignedSize { get { return alignedSize; } }

        internal bool Used { get { return used; } }

        internal string Filename { get { return filename; } }

        internal byte[] RandomGarbage 
        {
            get { return randomGarbage; }
            set
            {
                randomGarbage = new byte[7];

                Buffer.BlockCopy(value, 0, randomGarbage, 0, randomGarbage.Length);
            }
        }
        internal byte[] Key
        {
            get { return key; }
            set
            {
                key = new byte[0x40];

                Buffer.BlockCopy(value, 0, key, 0, key.Length);
            }
        }
        internal byte[] HMACHash1
        {
            get { return hmacHash1; }
            set
            {
                hmacHash1 = new byte[0x14];

                Buffer.BlockCopy(value, 0, hmacHash1, 0, hmacHash1.Length);
            }
        }
        internal byte[] HMACHash2
        {
            get { return hmacHash2; }
            set
            {
                hmacHash2 = new byte[0x14];

                Buffer.BlockCopy(value, 0, hmacHash2, 0, hmacHash2.Length);
            }
        }
        internal byte[] HMACHash3
        {
            get { return hmacHash3; }
            set
            {
                hmacHash3 = new byte[0x14];

                Buffer.BlockCopy(value, 0, hmacHash3, 0, hmacHash3.Length);
            }
        }
        internal byte[] HMACHash4
        {
            get { return hmacHash4; }
            set
            {
                hmacHash4 = new byte[0x14];

                Buffer.BlockCopy(value, 0, hmacHash4, 0, hmacHash4.Length);
            }
        }
        internal byte[] Padding
        {
            get { return padding; }
            set
            {
                padding = new byte[0x28];

                Buffer.BlockCopy(value, 0, padding, 0, padding.Length);
            }
        }
        internal byte[] HashKey
        {
            get { return hashKey; }
            set
            {
                hashKey = new byte[0x14];

                Buffer.BlockCopy(value, 0, hashKey, 0, hashKey.Length);
            }
        }
        internal byte[] DecryptionKey { get { return decryptionKey; } }
        internal byte[] ConsoleID
        {
            get { return consoleID; }
            set
            {
                consoleID = new byte[0x20];

                Buffer.BlockCopy(value, 0, consoleID, 0, consoleID.Length);
            }
        }

        internal byte[] HashData { get { return hashData; } }

        internal List<byte[]> HMACHashes { get { return hmacHashes; } }

        #endregion

        #region Private Methods

        private void CheckEndianness(ref byte[] value)
        {
            if (BitConverter.IsLittleEndian)
                Array.Reverse(value);
        }

        private long CheckEndianness(long value)
        {
            byte[] tmp = BitConverter.GetBytes(value);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(tmp);

            return BitConverter.ToInt64(tmp, 0);
        }

        private void SetSecureFileIDHashKey(byte[] value)
        {
            byte[] hashKey = new byte[0x14];

            Buffer.BlockCopy(value, 0, hashKey, 0, value.Length);

            for (int i = 0, x = 0; i < hashKey.Length; i++)
                switch (i)
                {
                    case 1:
                        hashKey[i] = 0xB;
                        break;

                    case 2:
                        hashKey[i] = 0xF;
                        break;

                    case 5:
                        hashKey[i] = 0xE;
                        break;

                    case 8:
                        hashKey[i] = 0xA;
                        break;

                    default:
                        hashKey[i] = value[x++];
                        break;
                }

            HashKey = hashKey;
        }

        private void SetParamSFOHashKey(int index)
        {
            switch(index)
            {
                case 0:
                    HashKey = saveGameParamSfoKey;
                    break;

                case 1:
                    HashKey = ConsoleID;
                    break;

                case 2:
                    HashKey = fallbackDiscHashKey;
                    break;

                case 3:
                    HashKey = authenticationID;
                    break;

                default:
                    HashKey = null;
                    break;
            }
        }

        private long AlignSize(long value)
        {
            return (value + 16 - 1) & ~(16 - 1);
        }

        #endregion

        #region Internal Methods

        internal void SetVirtualIndexID(byte[] value)
        {
            CheckEndianness(ref value);

            virtualIndexID = BitConverter.ToInt64(value, 0);

            used = VirtualIndexID == 0x72 ? false : true;
        }

        internal void SetFilename(byte[] value)
        {
            filename = Encoding.UTF8.GetString(value).Replace("\0", "");
        }

        internal void SetFilesize(byte[] value)
        {
            CheckEndianness(ref value);

            filesize = BitConverter.ToInt64(value, 0);

            alignedSize = AlignSize(Filesize);
        }

        internal void SetHashKey(int index, byte[] value)
        {
            switch (Filename.ToLower())
            {
                case "param.sfo":
                    SetParamSFOHashKey(index);
                    break;

                case "tropsys.dat":
                    HashKey = tropSysDatKey;
                    break;

                case "tropusr.dat":
                    HashKey = tropUsrDatKey;
                    break;

                case "troptrns.dat":
                    HashKey = tropTrnsDatKey;
                    break;

                case "tropconf.sfm":
                    HashKey = tropConfSfmKey;
                    break;

                default:
                    SetSecureFileIDHashKey(value);
                    break;
            }
        }

        internal void SetDecryptionKey(byte[] value)
        {
            byte[] smallHashKey = new byte[0x10];

            Buffer.BlockCopy(HashKey, 0, smallHashKey, 0, smallHashKey.Length);

            decryptionKey = Cryptographic_Engines.Decrypt(Key, value, smallHashKey, CipherMode.CBC, PaddingMode.Zeros);
        }

        internal void SetHashData()
        {
            using (MemoryStream ms = new MemoryStream())
            using (BinaryWriter bw = new BinaryWriter(ms))
            {
                byte[] rawFilename = Encoding.UTF8.GetBytes(Filename);

                byte[] name = new byte[0x41];
                Buffer.BlockCopy(rawFilename, 0, name, 0, Filename.Length);

                bw.Write(name);
                bw.Write(Key);
                
                for (int i = 0; i < HMACHashes.Count; i++)
                    bw.Write(HMACHashes[i]);

                bw.Write(Padding);

                long size = CheckEndianness(Filesize);
                bw.Write(size);

                hashData = ms.ToArray();
            }
        }

        internal void SetHMACHashes()
        {
            hmacHashes = new List<byte[]>(4);

            hmacHashes.Add(HMACHash1);
            hmacHashes.Add(HMACHash2);
            hmacHashes.Add(HMACHash3);
            hmacHashes.Add(HMACHash4);
        }

        #endregion
    }
}
