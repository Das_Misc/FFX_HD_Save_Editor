﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace Saves_Reencryptor
{
    class PS3_Save_Reencryptor
    {
        #region Fields

        private PFD_Header pfd;
        private SFO_Header sfo;

        private static readonly byte[] fallbackDiscHashKey = { 0xD1, 0xC1, 0xE1, 0x0B, 0x9C, 0x54, 0x7E, 0x68, 0x9B, 0x80, 0x5D, 0xCD, 0x97, 0x10, 0xCE, 0x8D };

        #endregion

        #region Properties

        internal PFD_Header PFD { get { return pfd; } }
        internal SFO_Header SFO { get { return sfo; } }

        #endregion

        #region Constructors

        internal PS3_Save_Reencryptor(string saveFolder) : this(saveFolder, fallbackDiscHashKey) { }

        internal PS3_Save_Reencryptor(string saveFolder, byte[] secureFileID)
        {
            sfo = new SFO_Header(saveFolder);

            if (secureFileID == null)
                secureFileID = fallbackDiscHashKey;

            pfd = new PFD_Header(saveFolder, secureFileID);
        }

        internal PS3_Save_Reencryptor(string saveFolder, string gamesConf)
        {
            sfo = new SFO_Header(saveFolder);

            string titleID = string.Empty;
            foreach (var entry in sfo.Entries)
                if (entry.KeyName == "SAVEDATA_DIRECTORY")
                {
                    titleID = entry.KeyValue.ToLower().Substring(0, 9);
                    break;
                }

            string[] contents = File.ReadAllLines(gamesConf);

            string rawSecureFileID = string.Empty;
            for (int i = 0; i < contents.Length; i++)
                if (contents[i].ToLower().Contains(titleID))
                    rawSecureFileID = contents[i + 2].Substring(17);

            byte[] secureFileID = new byte[0x10];

            if (rawSecureFileID == string.Empty)
                secureFileID = fallbackDiscHashKey;

            else
                for (int i = 0; i < secureFileID.Length; i++)
                {
                    string @byte = rawSecureFileID.Substring(i * 2, 2);
                    secureFileID[i] = Convert.ToByte(@byte, 16);
                }

            pfd = new PFD_Header(saveFolder, secureFileID);
        }

        #endregion

        #region Internal Methods

        /// <summary>
        /// Provides a few methods for different PARAM.SFO-related operations.
        /// After completing the requested operation, PARAM.SFO and PARAM.PFD will both be updated.
        /// </summary>
        /// <param name="operation">Must be ChangeAccountID, ChangePSID, ChangeRegion or ChangeUserID</param>
        /// <param name="newValue">AccountID 16 characters, PSID 32 characters, Region 9 characters, UserID from 1 to 2147483647</param>
        internal void ParamSFOMethods(string operation, string newValue)
        {
            var ChangeAccountID = new Action<string>(newAccountID =>
            {
                if (string.IsNullOrEmpty(newAccountID))
                    newAccountID = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";

                foreach (var entry in SFO.Entries)
                {
                    if (entry.KeyName == "ACCOUNT_ID")
                        entry.KeyValue = newAccountID;

                    if (entry.KeyName == "PARAMS")
                        entry.Param.AccountID = newAccountID;
                }
            });

            var ChangePSID = new Action<string>(newPSID =>
            {
                if (string.IsNullOrEmpty(newPSID))
                    newPSID = "00000000000000000000000000000000";

                foreach (var entry in SFO.Entries)
                    if (entry.KeyName == "PARAMS")
                        entry.Param.PSID = newPSID;
            });

            var ChangeRegion = new Action<string>(newTitleID =>
            {
                foreach (var entry in SFO.Entries)
                    if (entry.KeyName == "SAVEDATA_DIRECTORY")
                    {
                        entry.KeyValue = Regex.Replace(entry.KeyValue, @"[A-Z]{4}[\d]{5}", newTitleID.ToUpper());
                        break;
                    }

                string basePath = Path.GetDirectoryName(sfo.Folder);
                string originalName = new DirectoryInfo(sfo.Folder).Name;
                string newName = Regex.Replace(originalName, @"[A-Z]{4}[\d]{5}", newTitleID.ToUpper());

                string newPath = Path.Combine(basePath, newName);

                Directory.Move(sfo.Folder, newPath);

                sfo.Folder = newPath;
                pfd.SaveFolder = newPath;
            });

            var ChangeUserID = new Action<int>(newUserID =>
            {
                foreach (var entry in SFO.Entries)
                    if (entry.KeyName == "PARAMS")
                        entry.Param.UserID = newUserID;
            });

            switch (operation)
            {
                case "ChangeAccountID":
                    ChangeAccountID(newValue);
                    break;

                case "ChangeRegion":
                    ChangeRegion(newValue);
                    break;

                case "ChangeUserID":
                    ChangeUserID(int.Parse(newValue));
                    break;

                case "ChangePSID":
                    ChangePSID(newValue);
                    break;

                default:
                    return;
            }

            RebuildParamSFO();
            RebuildParamPFD();
        }

        /// <summary>
        /// Provides a few methods for nulling some PARAM.SFO-related properties.
        /// After completing the requested operation, PARAM.SFO and PARAM.PFD will both be updated.
        /// </summary>
        /// <param name="operation">Must be ChangeAccountID or ChangePSID</param>
        internal void ParamSFOMethods(string operation)
        {
            var ChangeAccountID = new Action(() =>
            {
                string newAccountID = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";

                foreach (var entry in SFO.Entries)
                {
                    if (entry.KeyName == "ACCOUNT_ID")
                        entry.KeyValue = newAccountID;

                    if (entry.KeyName == "PARAMS")
                        entry.Param.AccountID = newAccountID;
                }
            });

            var ChangePSID = new Action(() =>
            {
                string newPSID = "00000000000000000000000000000000";

                foreach (var entry in SFO.Entries)
                    if (entry.KeyName == "PARAMS")
                        entry.Param.PSID = newPSID;
            });

            switch (operation)
            {
                case "ChangeAccountID":
                    ChangeAccountID();
                    break;

                case "ChangePSID":
                    ChangePSID();
                    break;

                default:
                    return;
            }

            RebuildParamSFO();
            RebuildParamPFD();
        }

        internal void DecryptSaveFolder()
        {
            foreach (var entry in pfd.Entries)
            {
                if (entry.Filename.ToLower() == "param.sfo")
                    continue;

                string entryPath = Path.Combine(pfd.SaveFolder, entry.Filename);

                byte[] content = File.ReadAllBytes(entryPath);

                if (content.Length < entry.AlignedSize || !IsValidEntryHMACHashes())
                    continue;

                byte[] encryptedData = new byte[entry.AlignedSize];

                Buffer.BlockCopy(content, 0, encryptedData, 0, encryptedData.Length);

                byte[] decryptedData = Decrypt(entry.DecryptionKey, encryptedData, (int)entry.Filesize);

                File.WriteAllBytes(entryPath, decryptedData);
            }
        }

        internal void EncryptSaveFolder()
        {
            foreach (var entry in pfd.Entries)
            {
                if (entry.Filename.ToLower() == "param.sfo")
                    continue;

                string entryPath = Path.Combine(pfd.SaveFolder, entry.Filename);

                byte[] content = File.ReadAllBytes(entryPath);

                if (content.Length >= entry.AlignedSize)
                    continue;

                byte[] encryptedData = new byte[entry.AlignedSize];

                Buffer.BlockCopy(content, 0, encryptedData, 0, content.Length);

                byte[] decryptedData = Encrypt(entry.DecryptionKey, encryptedData, encryptedData.Length);

                File.WriteAllBytes(entryPath, decryptedData);
            }

            RebuildParamPFD();
        }

        #endregion

        #region Private Methods

        private void RebuildParamPFD()
        {
            FixEntryHMACHashes();
            FixDHKCID2();
            FixFileCID();
            FixPFDXYEntriesHash();
            FixPFDHash();

            byte[] content = new byte[PFD.FileSize];

            using (MemoryStream ms = new MemoryStream(content))
            using (BinaryWriter bw = new BinaryWriter(ms))
            {
                bw.Write(PFD_Header.Magic);
                bw.Write(pfd.Version);
                bw.Write(pfd.PFDHeaderTableIV);

                byte[] header = new byte[0x40];
                Buffer.BlockCopy(pfd.Hash1, 0, header, 0, pfd.Hash1.Length);
                Buffer.BlockCopy(pfd.Hash2, 0, header, pfd.Hash1.Length, pfd.Hash2.Length);
                Buffer.BlockCopy(pfd.HashKey, 0, header, pfd.Hash1.Length + pfd.Hash2.Length, pfd.HashKey.Length);
                Buffer.BlockCopy(pfd.Padding, 0, header, pfd.Hash1.Length + pfd.Hash2.Length + pfd.HashKey.Length, pfd.Padding.Length);

                byte[] pfdHeader = Cryptographic_Engines.Encrypt(header, PFD_Header.Syscon_iso_2, pfd.PFDHeaderTableIV, CipherMode.CBC, PaddingMode.Zeros);
                bw.Write(pfdHeader);

                bw.Write(CheckEndianness(pfd.XYReservedEntries));
                bw.Write(CheckEndianness(pfd.TotalReservedEntries));
                bw.Write(CheckEndianness(pfd.UsedEntries));

                ms.Position = 0x78;
                foreach (var xyEntry in pfd.XYEntries)
                    bw.Write(CheckEndianness(xyEntry.VirtualIndexID));

                ms.Position = 0x240;
                foreach (var entry in pfd.Entries)
                {
                    string entryPath = Path.Combine(pfd.SaveFolder, entry.Filename);
                    byte[] entryContent = File.ReadAllBytes(entryPath);

                    bw.Write(CheckEndianness(entry.VirtualIndexID));
                    bw.Write(Encoding.UTF8.GetBytes(entry.Filename));
                    bw.Write(new byte[0x41 - Encoding.UTF8.GetBytes(entry.Filename).Length]);
                    bw.Write(entry.RandomGarbage);
                    bw.Write(entry.Key);
                    foreach (var hmacHash in entry.HMACHashes)
                        bw.Write(hmacHash);
                    bw.Write(entry.Padding);
                    bw.Write(CheckEndianness(entry.Filesize));
                }

                ms.Position = 0x7B60;
                foreach (var xyEntry in pfd.XYEntries)
                    bw.Write(xyEntry.HMACHash);

                string paramPFDPath = Path.Combine(pfd.SaveFolder, pfd.ParamPFD);
                File.WriteAllBytes(paramPFDPath, ms.ToArray());
            }
        }

        private void RebuildParamSFO()
        {
            byte[] content = new byte[SFO.FileSize];

            using (MemoryStream ms = new MemoryStream(content))
            using (BinaryWriter bw = new BinaryWriter(ms))
            {
                bw.Write(SFO_Header.Magic);
                bw.Write(SFO.Version);
                bw.Write(SFO.KeyTableOffset);
                bw.Write(SFO.DataTableOffset);
                bw.Write(SFO.TablesEntriesCount);

                foreach (var entry in sfo.Entries)
                {
                    bw.Write(entry.KeyOffset);
                    bw.Write(entry.DataFormat);
                    bw.Write(entry.DataLength);
                    bw.Write(entry.DataMaxLength);
                    bw.Write(entry.DataOffset);
                }

                foreach (var entry in sfo.Entries)
                {
                    bw.Write(Encoding.UTF8.GetBytes(entry.KeyName)); // Avoid writing the prefixed string length
                    ms.WriteByte(0x00);
                }

                byte[] padding = new byte[sfo.DataTableOffset - ms.Position];
                bw.Write(padding);

                foreach (var entry in sfo.Entries)
                {
                    uint written = entry.DataLength;

                    if (entry.KeyName == "PARAMS")
                        bw.Write(entry.Param.RawContents);

                    else if (entry.DataFormat == 0x4)
                        bw.Write(Encoding.UTF8.GetBytes(entry.KeyValue));

                    else if (entry.DataFormat == 0x204)
                    {
                        bw.Write(Encoding.UTF8.GetBytes(entry.KeyValue));
                        written = (uint)entry.KeyValue.Length;
                    }

                    else if (entry.DataFormat == 0x404)
                        bw.Write(uint.Parse(entry.KeyValue));
                    
                    padding = new byte[entry.DataMaxLength - written];
                    bw.Write(padding);
                }

                string paramSFOPath = Path.Combine(sfo.Folder, sfo.ParamSFO);
                File.WriteAllBytes(paramSFOPath, ms.ToArray());
            }
        }

        private bool IsValidEntryHMACHashes()
        {
            for (int i = 0; i < pfd.Entries.Length; i++)
            {
                string entryPath = Path.Combine(pfd.SaveFolder, pfd.Entries[i].Filename);
                byte[] entryContents = File.ReadAllBytes(entryPath);

                var hmacHash = new HMACSHA1(pfd.Entries[i].HashKey).ComputeHash(entryContents);

                if (!pfd.Entries[i].HMACHashes[0].SequenceEqual(hmacHash))
                    return false;
            }

            return true;
        }

        private void FixEntryHMACHashes()
        {
            for (int i = 0; i < pfd.Entries.Length; i++)
            {
                string entryPath = Path.Combine(pfd.SaveFolder, pfd.Entries[i].Filename);
                byte[] entryContents = File.ReadAllBytes(entryPath);

                var hmacHash = new HMACSHA1(pfd.Entries[i].HashKey).ComputeHash(entryContents);

                if (!pfd.Entries[i].HMACHashes[0].SequenceEqual(hmacHash))
                    pfd.Entries[i].HMACHashes[0] = hmacHash;
            }
        }

        private void FixDHKCID2()
        {
            for (int i = 0; i < pfd.Entries.Length; i++)
            {
                int tableIndex = GetTableIndex(pfd.Entries[i]);

                byte[] hash = GetEntryHash(tableIndex);

                if (!pfd.XYEntries[tableIndex].HMACHash.SequenceEqual(hash))
                    pfd.XYEntries[tableIndex].HMACHash = hash;
            }
        }

        private int GetTableIndex(PFD_Entry entry)
        {
            long filenameHash = 0;
            for (int x = 0; x < entry.Filename.Length; x++)
                filenameHash = (filenameHash << 5) - filenameHash + (byte)entry.Filename[x];

            return (int)(filenameHash % pfd.XYReservedEntries);
        }

        private void FixFileCID()
        {
            byte[] buffer = new HMACSHA1(pfd.Key).ComputeHash(new byte[] { });

            if (buffer == null)
                return;

            List<int> tableIndexes = new List<int>();

            foreach (PFD_Entry entry in pfd.Entries)
            {
                int tableIndex = GetTableIndex(entry);

                tableIndexes.Add(tableIndex);
            }

            for (int i = 0; i < pfd.XYEntries.Length; i++)
            {
                if (tableIndexes.IndexOf(i) > -1)
                    continue;

                if (!pfd.XYEntries[i].HMACHash.SequenceEqual(buffer))
                    pfd.XYEntries[i].HMACHash = buffer;
            }
        }

        private void FixPFDHash()
        {
            byte[] hash = new HMACSHA1(pfd.Key).ComputeHash(pfd.HashData, 0, pfd.HashData.Length);

            if (!pfd.Hash2.SequenceEqual(hash))
                pfd.Hash2 = hash;
        }

        private void FixPFDXYEntriesHash()
        {
            byte[] PFDXYEntriesHashData = new byte[pfd.XYReservedEntries * 0x14];

            for (int i = 0; i < pfd.XYReservedEntries; i++)
                Buffer.BlockCopy(pfd.XYEntries[i].HMACHash, 0, PFDXYEntriesHashData, i * 0x14, pfd.XYEntries[i].HMACHash.Length);

            byte[] hash = new HMACSHA1(pfd.Key).ComputeHash(PFDXYEntriesHashData, 0, PFDXYEntriesHashData.Length);

            if (!pfd.Hash1.SequenceEqual(hash))
                pfd.Hash1 = hash;
        }

        private byte[] GetEntryHash(int tableIndex)
        {
            long currentEntryVirtualIndexID = pfd.XYEntries[tableIndex].VirtualIndexID;

            if (currentEntryVirtualIndexID < pfd.TotalReservedEntries)
            {
                HMACSHA1 sha1 = new HMACSHA1(pfd.Key);

                for (int i = 0; i < pfd.Entries.Length; i++)
                    pfd.Entries[i].SetHashData();

                List<byte> hashData = new List<byte>();

                while (currentEntryVirtualIndexID < pfd.TotalReservedEntries)
                {
                    PFD_Entry entry = pfd.Entries[currentEntryVirtualIndexID];

                    hashData.AddRange(entry.HashData);

                    currentEntryVirtualIndexID = entry.VirtualIndexID;
                }

                sha1.ComputeHash(hashData.ToArray());

                return sha1.Hash;
            }

            return null;
        }

        private byte[] Decrypt(byte[] decryptionKey, byte[] data, int size)
        {
            Array.Resize(ref decryptionKey, 0x10);

            byte[] buffer = new byte[data.Length];
            for (long i = 0; i < buffer.Length / 0x10; i++)
            {
                byte[] tmp = BitConverter.GetBytes(i);
                if (BitConverter.IsLittleEndian)
                    Array.Reverse(tmp);

                Buffer.BlockCopy(tmp, 0, buffer, (int)(i * 0x10), tmp.Length);
            }

            byte[] xor = Cryptographic_Engines.Encrypt(buffer, decryptionKey, decryptionKey, CipherMode.ECB, PaddingMode.Zeros);
            byte[] partiallyDecryptedData = Cryptographic_Engines.Decrypt(data, decryptionKey, decryptionKey, CipherMode.ECB, PaddingMode.Zeros);

            byte[] decryptedData = Cryptographic_Engines.Xor(partiallyDecryptedData, xor);

            Array.Resize(ref decryptedData, size);

            return decryptedData;
        }

        private byte[] Encrypt(byte[] decryptionKey, byte[] data, int size)
        {
            Array.Resize(ref decryptionKey, 0x10);

            byte[] buffer = new byte[data.Length];
            for (long i = 0; i < buffer.Length / 0x10; i++)
            {
                byte[] tmp = BitConverter.GetBytes(i);
                if (BitConverter.IsLittleEndian)
                    Array.Reverse(tmp);

                Buffer.BlockCopy(tmp, 0, buffer, (int)(i * 0x10), tmp.Length);
            }

            byte[] xor = Cryptographic_Engines.Encrypt(buffer, decryptionKey, decryptionKey, CipherMode.ECB, PaddingMode.Zeros);
            byte[] partiallyEncryptedData = Cryptographic_Engines.Xor(data, xor);

            byte[] encryptedData = Cryptographic_Engines.Encrypt(partiallyEncryptedData, decryptionKey, decryptionKey, CipherMode.ECB, PaddingMode.Zeros);
            
            return encryptedData;
        }
        
        private long CheckEndianness(long value)
        {
            byte[] tmp = BitConverter.GetBytes(value);

            if (BitConverter.IsLittleEndian)
                Array.Reverse(tmp);

            return BitConverter.ToInt64(tmp, 0);
        }

        #endregion
    }
}
