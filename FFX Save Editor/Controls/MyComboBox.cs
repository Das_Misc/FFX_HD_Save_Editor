﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace FFX_HD_Save_Editor.Controls
{
    class MyComboBox : ComboBox
    {
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            var popup = (Popup)Template.FindName("PART_Popup", this);
            popup.Placement = PlacementMode.Custom;
            popup.CustomPopupPlacementCallback = placePopup;
        }

        private CustomPopupPlacement[] placePopup(Size popupSize, Size targetSize, Point offset)
	{
		var placements = new[] { new CustomPopupPlacement() };
        placements[0].Point = new Point(ActualWidth, 0); // position the drop-down here!
		return placements;
	}
    }
}
