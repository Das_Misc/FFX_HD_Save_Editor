﻿using FFX_HD_Save_Editor.Models;
using Microsoft.Win32;
using Saves_Reencryptor;
using Shared_Classes;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;

namespace FFX_HD_Save_Editor.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        internal FFXSaveInfo ffxsi;
        private readonly byte[] ffxSecureFileID = { 0x02, 0x06, 0x04, 0x03, 0x03, 0x08, 0x03, 0x02, 0x07, 0x09, 0x05, 0x00, 0x02, 0x08, 0x08, 0x04 };

        private byte platform;

        private string saveFile, saveFolder;
        public ObservableCollection<GameItem> _inventory1 { get; set; }
        public ObservableCollection<GameItem> _inventory2 { get; set; }
        public ObservableCollection<KeyItem> _kiInventory1 { get; set; }
        public ObservableCollection<KeyItem> _kiInventory2 { get; set; }
        public ObservableCollection<Overdrive> _overdrives { get; set; }

        public MainWindow()
        {
            ffxsi = new FFXSaveInfo();
            ffxsi.SaveLoaded = false;
            DataContext = ffxsi;

            InitializeComponent();

            _inventory1 = new ObservableCollection<GameItem>();
            _inventory2 = new ObservableCollection<GameItem>();
            _kiInventory1 = new ObservableCollection<KeyItem>();
            _kiInventory2 = new ObservableCollection<KeyItem>();
            _overdrives = new ObservableCollection<Overdrive>();

            charsComboBox.SelectionChanged += cb_SelectionChanged;
            aeonsComboBox.SelectionChanged += aeonsComboBox_SelectionChanged;
            playersComboBox.SelectionChanged += playersComboBox_SelectionChanged;
            yojimboOptionComboBox.SelectionChanged += yojimboOptionComboBox_SelectionChanged;
        }

        private static void BitShiftLoad(BinaryReader br, dynamic stringList, dynamic objectList)
        {
            byte @byte = 0;
            for (int offset = 0, index = 0; index < Enumerable.Count(stringList); index++)
            {
                if (offset == 0)
                    @byte = br.ReadByte();

                byte bit = (byte)((@byte >> offset++) & 1);

                objectList[index].Bit = bit == 1 ? true : false;
                if (objectList is Overdrive[])
                    objectList[index].Name = stringList[index];
                else if (objectList is KeyItem[])
                    objectList[index].Item = stringList[index];

                offset %= 8;
            }
        }

        private void About(object sender, RoutedEventArgs e)
        {
            Assembly assembly = Assembly.GetEntryAssembly();

            string message = string.Format("{0}{3}v{1}{3}by {2}", assembly.GetName().Name, assembly.GetName().Version,
                                            assembly.GetCustomAttributes(typeof(AssemblyCompanyAttribute), false)
                                            .OfType<AssemblyCompanyAttribute>()
                                            .FirstOrDefault()
                                            .Company,
                                            Environment.NewLine);

            MessageBox.Show(message, "About...", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void aeonsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as ComboBox).SelectedItem != null)
                switch ((sender as ComboBox).SelectedItem.ToString())
                {
                    case "Valefor":
                        ffxsi.SelectedAeon = ffxsi.Valefor;
                        break;

                    case "Ifrit":
                        ffxsi.SelectedAeon = ffxsi.Ifrit;
                        break;

                    case "Ixion":
                        ffxsi.SelectedAeon = ffxsi.Ixion;
                        break;

                    case "Shiva":
                        ffxsi.SelectedAeon = ffxsi.Shiva;
                        break;

                    case "Bahamut":
                        ffxsi.SelectedAeon = ffxsi.Bahamut;
                        break;

                    case "Anima":
                        ffxsi.SelectedAeon = ffxsi.Anima;
                        break;

                    case "Yojimbo":
                        ffxsi.SelectedAeon = ffxsi.Yojimbo;
                        break;

                    case "Cindy":
                        ffxsi.SelectedAeon = ffxsi.Cindy;
                        break;

                    case "Sandy":
                        ffxsi.SelectedAeon = ffxsi.Sandy;
                        break;

                    case "Mindy":
                        ffxsi.SelectedAeon = ffxsi.Mindy;
                        break;
                }
        }

        private void BitShiftSave(BinaryWriter bw, dynamic objectList)
        {
            byte @byte = 0;
            for (int offset = 0, index = 0; index < Enumerable.Count(objectList); index++)
            {
                @byte |= (byte)((objectList[index].Bit ? 1 : 0) << offset++);

                offset %= index < 0x30 ? 8 : 4;

                if (offset == 0)
                {
                    bw.Write(@byte);
                    @byte = 0;
                }
            }
        }

        private void BitShiftSaveAlBhed(BinaryWriter bw, KeyItem[] keyItems)
        {
            byte @byte = 0;
            for (int offset = 0, index = 4; index < 30; index++)
            {
                @byte |= (byte)((keyItems[index].Bit ? 1 : 0) << offset++);

                offset %= 8;

                if (offset == 0 || index == 0x1D)
                {
                    bw.Write(@byte);
                    @byte = 0;
                }
            }
        }

        private void cb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as ComboBox).SelectedItem != null)
            {
                Binding[] od = new Binding[_overdrives.Count];
                for (int i = 0; i < od.Length; i++)
                {
                    od[i] = new Binding("[" + i + "].Bit");
                    od[i].Source = _overdrives;
                }

                var tbStyle = FindResource("MyToggleButton") as Style;
                ToggleButton[] tb = new ToggleButton[_overdrives.Count];
                for (int i = 0; i < tb.Length; i++)
                {
                    tb[i] = new ToggleButton();
                    tb[i].Style = tbStyle;
                    tb[i].Margin = i % 2 != 0 && i == 0 ? new Thickness(0, 0, 0, 0) : new Thickness(0, 5, 0, 0);
                    tb[i].Content = _overdrives[i].Name;
                    tb[i].SetBinding(ToggleButton.IsCheckedProperty, od[i]);
                }

                OverdrivesCompoundBox.Grid.Children.Clear();
                OverdrivesCompoundBox.Grid.ColumnDefinitions.Clear();
                OverdrivesCompoundBox.Grid.RowDefinitions.Clear();

                ColumnDefinition[] col = new ColumnDefinition[3];
                for (int i = 0; i < col.Length; i++)
                {
                    col[i] = new ColumnDefinition();
                    if (i % 2 == 0)
                        col[i].Width = new GridLength(1, GridUnitType.Star);
                    else
                        col[i].Width = new GridLength(10, GridUnitType.Pixel);

                    OverdrivesCompoundBox.Grid.ColumnDefinitions.Add(col[i]);
                }

                RowDefinition[] row = new RowDefinition[2];
                for (int i = 0; i < row.Length; i++)
                {
                    row[i] = new RowDefinition();

                    OverdrivesCompoundBox.Grid.RowDefinitions.Add(row[i]);
                }

                switch ((sender as ComboBox).SelectedItem.ToString())
                {
                    case "Tidus":
                        ffxsi.SelectedCharacter = ffxsi.Tidus;

                        Grid.SetColumn(tb[1], 2);
                        Grid.SetRow(tb[2], 1);
                        Grid.SetColumn(tb[3], 2);
                        Grid.SetRow(tb[3], 1);

                        for (int i = 0; i < 4; i++)
                            OverdrivesCompoundBox.Grid.Children.Add(tb[i]);

                        break;

                    case "Yuna":
                        ffxsi.SelectedCharacter = ffxsi.Yuna;
                        break;

                    case "Auron":
                        ffxsi.SelectedCharacter = ffxsi.Auron;

                        Grid.SetColumn(tb[5], 2);
                        Grid.SetRow(tb[6], 1);
                        Grid.SetColumn(tb[7], 2);
                        Grid.SetRow(tb[7], 1);

                        for (int i = 4; i < 8; i++)
                            OverdrivesCompoundBox.Grid.Children.Add(tb[i]);

                        break;

                    case "Kimahri":
                        ffxsi.SelectedCharacter = ffxsi.Kimahri;

                        RowDefinition[] rowExtra = new RowDefinition[5];
                        for (int i = 0; i < rowExtra.Length; i++)
                        {
                            rowExtra[i] = new RowDefinition();

                            OverdrivesCompoundBox.Grid.RowDefinitions.Add(rowExtra[i]);
                        }

                        Grid.SetColumn(tb[9], 2);
                        Grid.SetRow(tb[10], 2);
                        Grid.SetColumn(tb[11], 2);
                        Grid.SetRow(tb[11], 2);
                        Grid.SetRow(tb[12], 3);
                        Grid.SetColumn(tb[13], 2);
                        Grid.SetRow(tb[13], 3);
                        Grid.SetRow(tb[14], 4);
                        Grid.SetColumn(tb[15], 2);
                        Grid.SetRow(tb[15], 4);
                        Grid.SetRow(tb[16], 5);
                        Grid.SetColumn(tb[17], 2);
                        Grid.SetRow(tb[17], 5);
                        Grid.SetRow(tb[18], 6);
                        Grid.SetColumn(tb[19], 2);
                        Grid.SetRow(tb[19], 6);

                        for (int i = 8; i < 20; i++)
                            OverdrivesCompoundBox.Grid.Children.Add(tb[i]);

                        break;

                    case "Wakka":
                        ffxsi.SelectedCharacter = ffxsi.Wakka;

                        Grid.SetColumn(tb[21], 2);
                        Grid.SetRow(tb[22], 1);
                        Grid.SetColumn(tb[23], 2);
                        Grid.SetRow(tb[23], 1);

                        for (int i = 20; i < _overdrives.Count; i++)
                            OverdrivesCompoundBox.Grid.Children.Add(tb[i]);

                        break;

                    case "Lulu":
                        ffxsi.SelectedCharacter = ffxsi.Lulu;
                        break;

                    case "Rikku":
                        ffxsi.SelectedCharacter = ffxsi.Rikku;
                        break;
                }
            }
        }

        private void CheckUpdates_Click(object sender, RoutedEventArgs e)
        {
            if (File.Exists("Updater.exe"))
            {
                Assembly thisAssembly = Assembly.GetExecutingAssembly();
                Process.Start("Updater.exe", string.Format("\"{0}\" \"{1}\" \"{2}\"", "Final Fantasy X HD Save Editor", thisAssembly.GetName().Version, Process.GetCurrentProcess().MainModule.FileName));
            }
            else
                MessageBox.Show("ERROR: Missing required dependency \"Updater.exe\"", "Account Dumper", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void Donation(object sender, RoutedEventArgs e)
        {
            string reference = "FFXHDSE";

            Process.Start("https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=serenitydiary%40gmail%2ecom&lc=ES&item_name=Donate%20to%20Dasanko&item_number=" + reference + "&currency_code=EUR&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted");
        }

        private void LoadAeonsData(MemoryStream stream, BinaryReader br)
        {
            Aeon[] aeons = { ffxsi.Valefor, ffxsi.Ifrit, ffxsi.Ixion, ffxsi.Shiva, ffxsi.Bahamut, ffxsi.Anima, ffxsi.Yojimbo, ffxsi.Cindy, ffxsi.Sandy, ffxsi.Mindy };

            stream.Position = 0x5AB0;
            for (int i = 0; i < aeons.Length; i++)
            {
                aeons[i].AddedHP = BitConverter.ToInt32(br.ReadBytes(4), 0);
                aeons[i].AddedHP = aeons[i].AddedHP - aeons[i].BaseHP;
                aeons[i].AddedMP = BitConverter.ToInt32(br.ReadBytes(4), 0);
                aeons[i].AddedMP = aeons[i].AddedMP - aeons[i].BaseMP;

                aeons[i].AddedStrength = br.ReadByte();
                aeons[i].AddedStrength = (byte)(aeons[i].AddedStrength - aeons[i].BaseStrength);
                aeons[i].AddedDefense = br.ReadByte();
                aeons[i].AddedDefense = (byte)(aeons[i].AddedDefense - aeons[i].BaseDefense);
                aeons[i].AddedMagic = br.ReadByte();
                aeons[i].AddedMagic = (byte)(aeons[i].AddedMagic - aeons[i].BaseMagic);
                aeons[i].AddedMagicDefense = br.ReadByte();
                aeons[i].AddedMagicDefense = (byte)(aeons[i].AddedMagicDefense - aeons[i].BaseMagicDefense);
                aeons[i].AddedAgility = br.ReadByte();
                aeons[i].AddedAgility = (byte)(aeons[i].AddedAgility - aeons[i].BaseAgility);
                aeons[i].AddedLuck = br.ReadByte();
                aeons[i].AddedLuck = (byte)(aeons[i].AddedLuck - aeons[i].BaseLuck);
                aeons[i].AddedEvasion = br.ReadByte();
                aeons[i].AddedEvasion = (byte)(aeons[i].AddedEvasion - aeons[i].BaseEvasion);
                aeons[i].AddedAccuracy = br.ReadByte();
                aeons[i].AddedAccuracy = (byte)(aeons[i].AddedAccuracy - aeons[i].BaseAccuracy);

                stream.Position += 0x10; // Discard

                aeons[i].BaseHP = BitConverter.ToInt32(br.ReadBytes(4), 0);
                aeons[i].BaseMP = BitConverter.ToInt32(br.ReadBytes(4), 0);
                aeons[i].BaseStrength = br.ReadByte();
                aeons[i].BaseDefense = br.ReadByte();
                aeons[i].BaseMagic = br.ReadByte();
                aeons[i].BaseMagicDefense = br.ReadByte();
                aeons[i].BaseAgility = br.ReadByte();
                aeons[i].BaseLuck = br.ReadByte();
                aeons[i].BaseEvasion = br.ReadByte();
                aeons[i].BaseAccuracy = br.ReadByte();

                stream.Position += 5; // Discard

                aeons[i].CurrentOverdriveGauge = br.ReadByte();
                aeons[i].MaxOverdriveGauge = br.ReadByte();

                stream.Position += 0x5D; // Discard
            }
        }

        private void LoadCharsData(MemoryStream stream, BinaryReader br)
        {
            Character[] chars = { ffxsi.Tidus, ffxsi.Yuna, ffxsi.Auron, ffxsi.Kimahri, ffxsi.Wakka, ffxsi.Lulu, ffxsi.Rikku };

            stream.Position = 0x6C;
            for (int i = 0; i < chars.Length; i++)
                chars[i].AffectionToTidus = BitConverter.ToInt32(br.ReadBytes(4), 0);

            stream.Position = 0x5610; // Displayed values: Max HP, Total Strength, etc
            for (int i = 0; i < chars.Length; i++)
            {
                chars[i].BaseHP = BitConverter.ToInt32(br.ReadBytes(4), 0);
                chars[i].BaseMP = BitConverter.ToInt32(br.ReadBytes(4), 0);
                chars[i].BaseStrength = br.ReadByte();
                chars[i].BaseDefense = br.ReadByte();
                chars[i].BaseMagic = br.ReadByte();
                chars[i].BaseMagicDefense = br.ReadByte();
                chars[i].BaseAgility = br.ReadByte();
                chars[i].BaseLuck = br.ReadByte();
                chars[i].BaseEvasion = br.ReadByte();
                chars[i].BaseAccuracy = br.ReadByte();
                stream.Position += 4;

                chars[i].AP = BitConverter.ToInt32(br.ReadBytes(4), 0);

                stream.Position += 8; // Current HP and MP, discard
                chars[i].AddedHP = BitConverter.ToInt32(br.ReadBytes(4), 0);
                chars[i].AddedHP = chars[i].AddedHP - chars[i].BaseHP;
                chars[i].AddedMP = BitConverter.ToInt32(br.ReadBytes(4), 0);
                chars[i].AddedMP = chars[i].AddedMP - chars[i].BaseMP;

                stream.Position += 3; // Unknown values
                chars[i].AddedStrength = br.ReadByte();
                chars[i].AddedStrength = (byte)(chars[i].AddedStrength - chars[i].BaseStrength);
                chars[i].AddedDefense = br.ReadByte();
                chars[i].AddedDefense = (byte)(chars[i].AddedDefense - chars[i].BaseDefense);
                chars[i].AddedMagic = br.ReadByte();
                chars[i].AddedMagic = (byte)(chars[i].AddedMagic - chars[i].BaseMagic);
                chars[i].AddedMagicDefense = br.ReadByte();
                chars[i].AddedMagicDefense = (byte)(chars[i].AddedMagicDefense - chars[i].BaseMagicDefense);
                chars[i].AddedAgility = br.ReadByte();
                chars[i].AddedAgility = (byte)(chars[i].AddedAgility - chars[i].BaseAgility);
                chars[i].AddedLuck = br.ReadByte();
                chars[i].AddedLuck = (byte)(chars[i].AddedLuck - chars[i].BaseLuck);
                chars[i].AddedEvasion = br.ReadByte();
                chars[i].AddedEvasion = (byte)(chars[i].AddedEvasion - chars[i].BaseEvasion);
                chars[i].AddedAccuracy = br.ReadByte();
                chars[i].AddedAccuracy = (byte)(chars[i].AddedAccuracy - chars[i].BaseAccuracy);

                chars[i].PercentDamageFromPoison = br.ReadByte();
                stream.Position++; // Unknown value
                chars[i].CurrentOverdriveGauge = br.ReadByte();
                chars[i].MaxOverdriveGauge = br.ReadByte();
                chars[i].CurrentSphereLevel = br.ReadByte();
                chars[i].TotalSphereLevel = br.ReadByte();

                stream.Position += 0x17; // Unknown values
                chars[i].EnemiesDefeated = BitConverter.ToInt32(br.ReadBytes(4), 0);

                stream.Position += 8; // Unknown values
                chars[i].TillWarriorOD = BitConverter.ToInt16(br.ReadBytes(2), 0);
                chars[i].TillComradeOD = BitConverter.ToInt16(br.ReadBytes(2), 0);
                stream.Position += 2; // Stoic, we don't care about this one because all characters start with it
                chars[i].TillHealerOD = BitConverter.ToInt16(br.ReadBytes(2), 0);
                chars[i].TillTacticianOD = BitConverter.ToInt16(br.ReadBytes(2), 0);
                chars[i].TillVictimOD = BitConverter.ToInt16(br.ReadBytes(2), 0);
                chars[i].TillDancerOD = BitConverter.ToInt16(br.ReadBytes(2), 0);
                chars[i].TillAvengerOD = BitConverter.ToInt16(br.ReadBytes(2), 0);
                chars[i].TillSlayerOD = BitConverter.ToInt16(br.ReadBytes(2), 0);
                chars[i].TillHeroOD = BitConverter.ToInt16(br.ReadBytes(2), 0);
                chars[i].TillRookOD = BitConverter.ToInt16(br.ReadBytes(2), 0);
                chars[i].TillVictorOD = BitConverter.ToInt16(br.ReadBytes(2), 0);
                chars[i].TillCowardOD = BitConverter.ToInt16(br.ReadBytes(2), 0);
                chars[i].TillAllyOD = BitConverter.ToInt16(br.ReadBytes(2), 0);
                chars[i].TillSuffererOD = BitConverter.ToInt16(br.ReadBytes(2), 0);
                chars[i].TillDaredevilOD = BitConverter.ToInt16(br.ReadBytes(2), 0);
                chars[i].TillLonerOD = BitConverter.ToInt16(br.ReadBytes(2), 0);

                stream.Position += 8; // Unknown values

                byte tmp = br.ReadByte();
                chars[i].AeonsOnlyOD = tmp == 8 || tmp == 9 ? true : false;

                stream.Position += 0xD; // Unknown values
            }
        }

        private void LoadItemsData(MemoryStream stream, BinaryReader br)
        {
            // The items' area is 0xE0 in length, but each item is separated by either 0x20 (there's an item) or 0x00 (there's no item)
            // Effectively, 0xE0 / 2 = 0x70
            const int INVENTORY_SIZE = 0x70;

            GameItem[] gameItems = new GameItem[INVENTORY_SIZE];
            for (int i = 0; i < gameItems.Length; i++)
                gameItems[i] = new GameItem(); // Initialize the collection

            stream.Position = 0x3F0C;

            for (int i = 0; i < INVENTORY_SIZE; i++)
            {
                gameItems[i].Item = br.ReadByte();
                br.ReadByte();
            }

            stream.Position = 0x410C;

            for (int i = 0; i < INVENTORY_SIZE; i++)
                gameItems[i].Quantity = br.ReadByte();

            for (int i = 0; i < gameItems.Length; i++)
            {
                if (i % 2 == 0)
                    _inventory1.Add(gameItems[i]);
                else
                    _inventory2.Add(gameItems[i]);
            }
        }

        private void LoadKeyItemsData(MemoryStream stream, BinaryReader br)
        {
            ListKeyItem keyItemList = new ListKeyItem();
            keyItemList.InternalOrder();

            KeyItem[] keyItems = new KeyItem[keyItemList.Count];
            for (int i = 0; i < keyItems.Length; i++)
                keyItems[i] = new KeyItem();

            stream.Position = 0x44CC;
            BitShiftLoad(br, keyItemList, keyItems);

            if (keyItems[2].Bit && keyItems[3].Bit) // BOTH cloudy mirror and celestial mirror? Can only hold one, so keep the celestial only
                keyItems[2].Bit = false;

            keyItemList = new ListKeyItem();
            var keyItemsList = keyItems.ToList();

            bool firstList = true;

            for (int i = 0; i < keyItemList.Count; i++)
            {
                foreach (var keyItem in keyItemsList)
                {
                    if (keyItemList[i] == keyItem.Item && firstList)
                    {
                        firstList = !firstList;
                        _kiInventory1.Add(keyItem);
                    }
                    else if (keyItemList[i] == keyItem.Item && !firstList)
                    {
                        firstList = !firstList;
                        _kiInventory2.Add(keyItem);
                    }
                }
            }
        }

        private void LoadOverdrivesData(MemoryStream stream, BinaryReader br)
        {
            stream.Position = 0x3DAC;
            ListOverdrive overdrivesList = new ListOverdrive();

            Overdrive[] overdrives = new Overdrive[overdrivesList.Count];
            for (int i = 0; i < overdrives.Length; i++)
                overdrives[i] = new Overdrive();

            BitShiftLoad(br, overdrivesList.ToArray(), overdrives);

            foreach (var overdrive in overdrives)
                _overdrives.Add(overdrive);
        }

        private void LoadPlayersData(MemoryStream stream, BinaryReader br)
        {
            BlitzPlayer[] players = { ffxsi.TidusB, ffxsi.WakkaB, ffxsi.DattoB, ffxsi.LettyB, ffxsi.JassuB, ffxsi.BottaB, ffxsi.KeepaB };

            stream.Position = 0x122C;
            for (int i = 0; i < players.Length; i++)
            {
                byte[] offensiveTechniquesBA = br.ReadBytes(4);
                Array.Reverse(offensiveTechniquesBA);
                players[i].OffensiveTechniques = BitConverter.ToUInt32(offensiveTechniquesBA, 0);
            }

            stream.Position = 0x131C;
            for (int i = 0; i < players.Length; i++)
            {
                byte[] defensiveTechniquesBA = br.ReadBytes(4);
                Array.Reverse(defensiveTechniquesBA);
                players[i].DefensiveTechniques = BitConverter.ToUInt32(defensiveTechniquesBA, 0);
            }

            for (int i = 0; i < players.Length; i++)
                players[i].LearnedTechniques = players[i].OffensiveTechniques + players[i].DefensiveTechniques;

            stream.Position = 0x15BE;
            for (int i = 0; i < players.Length; i++)
                players[i].TechniqueSlots = br.ReadByte();

            stream.Position = 0x15FA;
            for (int i = 0; i < players.Length; i++)
                players[i].Level = br.ReadByte();

            stream.Position = 0x1756;
            for (int i = 0; i < players.Length; i++)
                players[i].ContractGamesLeft = br.ReadByte();

            stream.Position = 0x1794;
            for (int i = 0; i < players.Length; i++)
                players[i].Experience = BitConverter.ToUInt16(br.ReadBytes(2), 0);
        }

        private void LoadSave()
        {
            byte[] saveFileContents = File.ReadAllBytes(saveFile);

            using (MemoryStream stream = new MemoryStream(saveFileContents))
            using (BinaryReader br = new BinaryReader(stream))
            {
                stream.Position = 0x2D4;
                ffxsi.WobblyChocoboMins = br.ReadByte();
                ffxsi.WobblyChocoboSecs = br.ReadByte();
                ffxsi.WobblyChocoboMils = br.ReadByte();
                ffxsi.DodgerChocoboMins = br.ReadByte();
                ffxsi.DodgerChocoboSecs = br.ReadByte();
                ffxsi.DodgerChocoboMils = br.ReadByte();
                ffxsi.HyperDodgerChocoboMins = br.ReadByte();
                ffxsi.HyperDodgerChocoboSecs = br.ReadByte();
                ffxsi.HyperDodgerChocoboMils = br.ReadByte();
                ffxsi.CatcherChocoboMins = br.ReadByte();
                ffxsi.CatcherChocoboSecs = br.ReadByte();
                ffxsi.CatcherChocoboMils = br.ReadByte();

                stream.Position = 0x430;
                ffxsi.PerformedJechtShot = br.ReadByte();

                stream.Position = 0x43C;
                ffxsi.HitByLightnings = BitConverter.ToUInt16(br.ReadBytes(2), 0);

                stream.Position = 0x440;
                ffxsi.DodgedLightningsInARow = BitConverter.ToUInt16(br.ReadBytes(2), 0);

                stream.Position = 0xC5C;
                ffxsi.BeatLucaGoers = br.ReadByte();

                stream.Position = 0xC60;
                ffxsi.GilDonated = BitConverter.ToSingle(br.ReadBytes(4), 0);

                stream.Position = 0xC75;
                ffxsi.UnlockedWorldChampion = br.ReadByte();

                stream.Position = 0xC79;
                ffxsi.Celestials = br.ReadByte();

                stream.Position = 0xC9C;
                ffxsi.AnimaUnlock = br.ReadByte();

                stream.Position = 0xCC3;
                ffxsi.CactuarStage = br.ReadByte();
                br.ReadByte(); // Padding byte, discard
                ffxsi.CactuarSpheres = br.ReadByte();

                stream.Position = 0x3D4D;
                ffxsi.SphereGridType = br.ReadByte(); // 0x6F for Standard Grid, 0xAF for Expert Grid

                stream.Position = 0x222C;
                int bytesToRead = ffxsi.SphereGridType == 0x6F ? 0x6B8 : 0x65C; // Standard Grid ranges from 0x222C to 0x28E3. Expert Grid ranges from 0x2254 to 0x2887 - bytes 0x2887-0x28E3 are set to null (0xFF)
                ffxsi.SphereGridContents = br.ReadBytes(bytesToRead);

                stream.Position = 0x3D54;
                ffxsi.Battles = BitConverter.ToUInt32(br.ReadBytes(4), 0);

                stream.Position = 0x3D88;
                ffxsi.Gil = BitConverter.ToUInt32(br.ReadBytes(4), 0);

                stream.Position = 0x3DE4;
                ffxsi.YojimboCompatibility = br.ReadByte();

                stream.Position = 0x3DE8;
                ffxsi.YojimboOption = br.ReadByte();

                stream.Position = 0x3DEC;
                ffxsi.TidusODUsed = BitConverter.ToUInt32(br.ReadBytes(4), 0);

                stream.Position = 0x424C;
                byte[] captured = br.ReadBytes(0x68);
                byte[] check = new byte[0x68];
                for (int i = 0; i < check.Length; i++)
                    check[i] = 0xA;
                ffxsi.ArenaMonstersUnlock = Enumerable.SequenceEqual(captured, check); // Set to true if both arrays match (all values are 0xA), else false

                LoadCharsData(stream, br);

                LoadAeonsData(stream, br);

                LoadOverdrivesData(stream, br);

                LoadPlayersData(stream, br);

                LoadItemsData(stream, br);

                LoadKeyItemsData(stream, br);
            }
        }

        private void MaximizeGridStatNodes()
        {
            for (int i = 0; i < ffxsi.SphereGridContents.Length; i++)
            {
                if (i % 2 != 0)
                    continue;

                switch (ffxsi.SphereGridContents[i])
                {
                    // 0x00 Empty

                    // Strength spheres
                    case 0x02: // 1
                    case 0x03: // 2
                    case 0x04: // 3
                        ffxsi.SphereGridContents[i] = 0x05; // 4
                        break;

                    // Defense spheres
                    case 0x06: // 1
                    case 0x07: // 2
                    case 0x08: // 3
                        ffxsi.SphereGridContents[i] = 0x09; // 4
                        break;

                    // Magic spheres
                    case 0x0A: // 1
                    case 0x0B: // 2
                    case 0x0C: // 3
                        ffxsi.SphereGridContents[i] = 0x0D; // 4
                        break;

                    // Magic Defense spheres
                    case 0x0E: // 1
                    case 0x0F: // 2
                    case 0x10: // 3
                        ffxsi.SphereGridContents[i] = 0x11; // 4
                        break;

                    // Agility spheres
                    case 0x12: // 1
                    case 0x13: // 2
                    case 0x14: // 3
                        ffxsi.SphereGridContents[i] = 0x15; // 4
                        break;

                    // Luck spheres
                    case 0x16: // 1
                    case 0x17: // 2
                    case 0x18: // 3
                        ffxsi.SphereGridContents[i] = 0x19; // 4
                        break;

                    // Evasion spheres
                    case 0x1A: // 1
                    case 0x1B: // 2
                    case 0x1C: // 3
                        ffxsi.SphereGridContents[i] = 0x1D; // 4
                        break;

                    // Accuracy spheres
                    case 0x1E: // 1
                    case 0x1F: // 2
                    case 0x20: // 3
                        ffxsi.SphereGridContents[i] = 0x21; // 4
                        break;

                    // HP Spheres
                    case 0x22: // 200
                        ffxsi.SphereGridContents[i] = 0x23; // 300
                        break;

                    // MP Spheres
                    case 0x26: // 10
                    case 0x25: // 20
                        ffxsi.SphereGridContents[i] = 0x24; // 40
                        break;
                }
            }
        }

        private void OpenPC(object sender, RoutedEventArgs e)
        {
            string documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string saveFolder = Path.Combine(documents, @"SQUARE ENIX\FINAL FANTASY X&X-2 HD Remaster\FINAL FANTASY X");

            var ofd = new OpenFileDialog();
            ofd.FileName = "ffx_*";
            ofd.InitialDirectory = saveFolder;

            if (ofd.ShowDialog().Value)
            {
                saveFile = ofd.FileName;

                ffxsi = new FFXSaveInfo();
                DataContext = ffxsi;
                ffxsi.SaveLoaded = true;

                _inventory1.Clear();
                _inventory2.Clear();
                _kiInventory1.Clear();
                _kiInventory2.Clear();
                _overdrives = new ObservableCollection<Overdrive>(); // Prevent dynamic binding errors

                LoadSave();

                var previous = charsComboBox.SelectedItem;
                charsComboBox.SelectedItem = null;
                charsComboBox.SelectedItem = previous == null ? "Tidus" : previous;

                previous = aeonsComboBox.SelectedItem;
                aeonsComboBox.SelectedItem = null;
                aeonsComboBox.SelectedItem = previous == null ? "Valefor" : previous;

                previous = playersComboBox.SelectedItem;
                playersComboBox.SelectedItem = null;
                playersComboBox.SelectedItem = previous == null ? "Tidus" : previous;

                platform = 0;
            }
        }

        private void OpenPS3(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.CheckFileExists = false;
            ofd.FileName = "Select this folder";

            if (ofd.ShowDialog().Value)
            {
                saveFolder = Path.GetDirectoryName(ofd.FileName);

                if (Directory.GetFiles(saveFolder).Any(x => x.Contains("saves", StringComparison.OrdinalIgnoreCase)))
                {
                    var reencryptor = new PS3_Save_Reencryptor(saveFolder, ffxSecureFileID);
                    reencryptor.DecryptSaveFolder();

                    saveFile = Path.Combine(saveFolder, "SAVES");

                    ffxsi = new FFXSaveInfo();
                    DataContext = ffxsi;
                    ffxsi.SaveLoaded = true;

                    _inventory1.Clear();
                    _inventory2.Clear();
                    _kiInventory1.Clear();
                    _kiInventory2.Clear();
                    _overdrives = new ObservableCollection<Overdrive>(); // Prevent dynamic binding errors

                    LoadSave();

                    var previous = charsComboBox.SelectedItem;
                    charsComboBox.SelectedItem = null;
                    charsComboBox.SelectedItem = previous == null ? "Tidus" : previous;

                    previous = aeonsComboBox.SelectedItem;
                    aeonsComboBox.SelectedItem = null;
                    aeonsComboBox.SelectedItem = previous == null ? "Valefor" : previous;

                    previous = playersComboBox.SelectedItem;
                    playersComboBox.SelectedItem = null;
                    playersComboBox.SelectedItem = previous == null ? "Tidus" : previous;

                    platform = 1;
                }
                else
                    MessageBox.Show("ERROR: The selected folder does not contain a valid PS3 Final Fantasy X HD Remaster save file.", "Final Fantasy X HD Save Editor", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void OpenPSV(object sender, RoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.FileName = "data*.bin";
            ofd.InitialDirectory = saveFolder;

            if (ofd.ShowDialog().Value)
            {
                saveFile = ofd.FileName;

                ffxsi = new FFXSaveInfo();
                DataContext = ffxsi;
                ffxsi.SaveLoaded = true;

                _inventory1.Clear();
                _inventory2.Clear();
                _kiInventory1.Clear();
                _kiInventory2.Clear();
                _overdrives = new ObservableCollection<Overdrive>(); // Prevent dynamic binding errors

                LoadSave();

                var previous = charsComboBox.SelectedItem;
                charsComboBox.SelectedItem = null;
                charsComboBox.SelectedItem = previous == null ? "Tidus" : previous;

                previous = aeonsComboBox.SelectedItem;
                aeonsComboBox.SelectedItem = null;
                aeonsComboBox.SelectedItem = previous == null ? "Valefor" : previous;

                previous = playersComboBox.SelectedItem;
                playersComboBox.SelectedItem = null;
                playersComboBox.SelectedItem = previous == null ? "Tidus" : previous;

                platform = 2;
            }
        }

        private void playersComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as ComboBox).SelectedItem != null)
                switch ((sender as ComboBox).SelectedItem.ToString())
                {
                    case "Tidus":
                        ffxsi.SelectedPlayer = ffxsi.TidusB;
                        break;

                    case "Wakka":
                        ffxsi.SelectedPlayer = ffxsi.WakkaB;
                        break;

                    case "Datto":
                        ffxsi.SelectedPlayer = ffxsi.DattoB;
                        break;

                    case "Letty":
                        ffxsi.SelectedPlayer = ffxsi.LettyB;
                        break;

                    case "Jassu":
                        ffxsi.SelectedPlayer = ffxsi.JassuB;
                        break;

                    case "Botta":
                        ffxsi.SelectedPlayer = ffxsi.BottaB;
                        break;

                    case "Keepa":
                        ffxsi.SelectedPlayer = ffxsi.KeepaB;
                        break;
                }
        }

        private void Save(object sender, RoutedEventArgs e)
        {
            byte[] rawContents = File.ReadAllBytes(saveFile);

            itemsDataGrid.CommitEdit(); // First finish whatever editing may be ongoing
            itemsDataGrid.CommitEdit(); // Next make sure we push those changes to the observable collection - this just seems to be a bug in WPF/.NET

            keyItemsDatagrid1.CommitEdit();
            keyItemsDatagrid1.CommitEdit();
            keyItemsDatagrid2.CommitEdit();
            keyItemsDatagrid2.CommitEdit();

            using (MemoryStream ms = new MemoryStream(rawContents))
            using (BinaryWriter bw = new BinaryWriter(ms))
            {
                if (ffxsi.NewGamePlus)
                {
                    ms.Position = 0xF8;
                    bw.Write(new byte[] { 0 });

                    ms.Position = 0xFA;
                    bw.Write(new byte[] { 0x84, 0 });

                    ms.Position = 0x42C;
                    bw.Write(new byte[] { 0 });

                    ms.Position = 0xC2C;
                    bw.Write(new byte[] { 0 });
                }

                byte chocoboTrainingCompleted = 0;
                if (ffxsi.WobblyChocoboCompleted)
                    chocoboTrainingCompleted += 0x2;
                if (ffxsi.DodgerChocoboCompleted)
                    chocoboTrainingCompleted += 0x8;
                if (ffxsi.HyperDodgerChocoboCompleted)
                    chocoboTrainingCompleted += 0x10;
                if (ffxsi.CatcherChocoboCompleted)
                {
                    ms.Position = 0x2BA;
                    bw.Write(0x10);
                    chocoboTrainingCompleted += 0x20;

                    ms.Position = 0x2E5;
                    bw.Write(0x1);
                }
                ms.Position = 0x2BB;
                bw.Write(chocoboTrainingCompleted);

                ms.Position = 0x2D4;
                bw.Write(ffxsi.WobblyChocoboMins);
                bw.Write(ffxsi.WobblyChocoboSecs);
                bw.Write(ffxsi.WobblyChocoboMils);
                bw.Write(ffxsi.DodgerChocoboMins);
                bw.Write(ffxsi.DodgerChocoboSecs);
                bw.Write(ffxsi.DodgerChocoboMils);
                bw.Write(ffxsi.HyperDodgerChocoboMins);
                bw.Write(ffxsi.HyperDodgerChocoboSecs);
                bw.Write(ffxsi.HyperDodgerChocoboMils);
                bw.Write(ffxsi.CatcherChocoboMins);
                bw.Write(ffxsi.CatcherChocoboSecs);
                bw.Write(ffxsi.CatcherChocoboMils);

                ms.Position = 0x430;
                bw.Write(ffxsi.PerformedJechtShot);

                ms.Position = 0x43C;
                bw.Write(ffxsi.HitByLightnings);
                bw.Write(ffxsi.DodgedLightningsInARow); // Total dodged lightnings, set to the same as the lightnings dodged in a row

                ms.Position = 0x440;
                bw.Write(ffxsi.DodgedLightningsInARow);

                ms.Position = 0xC5C;
                bw.Write(ffxsi.BeatLucaGoers);

                ms.Position = 0xC60;
                bw.Write(ffxsi.GilDonated);

                ms.Position = 0xC68;
                byte performed = (byte)(ffxsi.PerformedJechtShot == 0x5E ? 1 : 0);
                bw.Write(performed);

                ms.Position = 0xC75;
                bw.Write(ffxsi.UnlockedWorldChampion);

                ms.Position = 0xC9C;
                bw.Write(ffxsi.AnimaUnlock);

                ms.Position = 0xCC3;
                bw.Write(ffxsi.CactuarStage);
                bw.Write(0); // Padding byte, ignore this
                bw.Write(ffxsi.CactuarSpheres);

                ms.Position = 0xCC5;
                bw.Write(ffxsi.CactuarSpheres);

                if (ffxsi.MaximizeStatNodes)
                {
                    ms.Position = 0x222C;

                    MaximizeGridStatNodes();

                    bw.Write(ffxsi.SphereGridContents);
                }

                ms.Position = 0x3D54;
                bw.Write(ffxsi.Battles);

                ms.Position = 0x3D88;
                bw.Write(ffxsi.Gil);

                ms.Position = 0x3DE4;
                bw.Write(ffxsi.YojimboCompatibility);

                ms.Position = 0x3DE8;
                bw.Write(ffxsi.YojimboOption);

                ms.Position = 0x3DEC;
                bw.Write(ffxsi.TidusODUsed);

                if (ffxsi.ArenaMonstersUnlock)
                {
                    byte[] check = new byte[0x68];
                    for (int i = 0; i < check.Length; i++)
                        check[i] = 0xA;

                    ms.Position = 0x424C;
                    bw.Write(check);
                }

                byte celestials = 0;
                if (ffxsi.AddUltima)
                {
                    ms.Position = 0x5572;
                    bw.Write(new byte[] { 0x00, 0x50, 0x01, 0x04, 0x00, 0x00, 0xFF, 0x00, 0x11, 0x10, 0x03, 0x04, 0x07, 0x40, 0x14, 0x80, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00 });
                    celestials += 0x01;
                }
                if (ffxsi.AddNirvana)
                {
                    ms.Position = 0x5588;
                    bw.Write(new byte[] { 0x00, 0x50, 0x01, 0x04, 0x01, 0x00, 0xFF, 0x00, 0x12, 0x10, 0x03, 0x04, 0x10, 0x40, 0x14, 0x80, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00 });
                    celestials += 0x02;
                }
                if (ffxsi.AddMasamune)
                {
                    ms.Position = 0x559E;
                    bw.Write(new byte[] { 0x00, 0x50, 0x01, 0x04, 0x02, 0x00, 0xFF, 0x00, 0x13, 0x10, 0x03, 0x04, 0x19, 0x40, 0x14, 0x80, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00 });
                    celestials += 0x08;
                }
                if (ffxsi.AddLonginus)
                {
                    ms.Position = 0x55B4;
                    bw.Write(new byte[] { 0x00, 0x50, 0x01, 0x04, 0x03, 0x00, 0xFF, 0x00, 0x11, 0x10, 0x03, 0x04, 0x24, 0x40, 0x14, 0x80, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00 });
                    celestials += 0x10;
                }
                if (ffxsi.AddWorld)
                {
                    ms.Position = 0x55CA;
                    bw.Write(new byte[] { 0x00, 0x50, 0x01, 0x04, 0x04, 0x00, 0xFF, 0x00, 0x11, 0x10, 0x03, 0x04, 0x2E, 0x40, 0x14, 0x80, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00 });
                    celestials += 0x04;
                }
                if (ffxsi.AddOnion)
                {
                    ms.Position = 0x55E0;
                    bw.Write(new byte[] { 0x00, 0x50, 0x01, 0x04, 0x05, 0x00, 0xFF, 0x00, 0x12, 0x10, 0x03, 0x04, 0x38, 0x40, 0x14, 0x80, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00 });
                    celestials += 0x20;
                }
                if (ffxsi.AddGodhand)
                {
                    ms.Position = 0x55F6;
                    bw.Write(new byte[] { 0x00, 0x50, 0x01, 0x04, 0x06, 0x00, 0xFF, 0x00, 0x11, 0x10, 0x03, 0x04, 0x41, 0x40, 0x14, 0x80, 0xFF, 0x00, 0xFF, 0x00, 0xFF, 0x00 });
                    celestials += 0x40;
                }

                if (celestials != 0)
                {
                    ms.Position = 0xC79;
                    byte celestial = ffxsi.Celestials + celestials > 0x7F ? (byte)0x7F : (byte)(ffxsi.Celestials + celestials);
                    bw.Write(celestial);
                }

                ms.Position = 0x3DAC; // Save Overdrives data
                BitShiftSave(bw, _overdrives);

                SaveCharsData(ms, bw);

                SaveAeonsData(ms, bw);

                SavePlayersData(ms, bw);

                SaveItemsData(ms, bw);

                SaveKeyItemsData(ms, bw);

                // Checksum fix
                ms.Position = 0x64F4;
                bw.Write(0);

                byte[] dataToHash = new byte[0x64B8]; // End (0x64F8) - beginning (0x40): 0x64B8
                Array.Copy(ms.ToArray(), 0x40, dataToHash, 0, dataToHash.Length);

                byte[] newChecksum = CRC16_CCITT.CRC16CCITT(dataToHash);

                ms.Position = 0x1A;
                bw.Write(newChecksum);

                ms.Position = 0x64F4;
                bw.Write(newChecksum);

                File.WriteAllBytes(saveFile, ms.ToArray());
            }

            if (platform == 1)
            {
                PS3_Save_Reencryptor reencryptor = new PS3_Save_Reencryptor(saveFolder, ffxSecureFileID);
                reencryptor.EncryptSaveFolder();
            }

            GlobalTab.Focus();
            ffxsi = new FFXSaveInfo();
            ffxsi.SaveLoaded = false;
            DataContext = ffxsi;
        }

        private void SaveAeonsData(MemoryStream stream, BinaryWriter bw)
        {
            Aeon[] aeons = { ffxsi.Valefor, ffxsi.Ifrit, ffxsi.Ixion, ffxsi.Shiva, ffxsi.Bahamut, ffxsi.Anima, ffxsi.Yojimbo, ffxsi.Cindy, ffxsi.Sandy, ffxsi.Mindy };

            stream.Position = 0x5AB0;
            for (int i = 0; i < aeons.Length; i++)
            {
                bw.Write(aeons[i].BaseHP);
                bw.Write(aeons[i].BaseMP);
                bw.Write(aeons[i].BaseStrength);
                bw.Write(aeons[i].BaseDefense);
                bw.Write(aeons[i].BaseMagic);
                bw.Write(aeons[i].BaseMagicDefense);
                bw.Write(aeons[i].BaseAgility);
                bw.Write(aeons[i].BaseLuck);
                bw.Write(aeons[i].BaseEvasion);
                bw.Write(aeons[i].BaseAccuracy);

                stream.Position += 0x10;// Discard

                bw.Write(aeons[i].TotalHP);
                bw.Write(aeons[i].TotalMP);

                stream.Position += 3; // Unknown

                bw.Write(aeons[i].TotalStrength);
                bw.Write(aeons[i].TotalDefense);
                bw.Write(aeons[i].TotalMagic);
                bw.Write(aeons[i].TotalMagicDefense);
                bw.Write(aeons[i].TotalAgility);
                bw.Write(aeons[i].TotalLuck);
                bw.Write(aeons[i].TotalEvasion);
                bw.Write(aeons[i].TotalAccuracy);

                stream.Position += 2; // Unknown

                bw.Write(aeons[i].CurrentOverdriveGauge);
                bw.Write(aeons[i].MaxOverdriveGauge);

                stream.Position += 0x5D; // Discard
            }
        }

        private void SaveCharsData(MemoryStream stream, BinaryWriter bw)
        {
            Character[] chars = { ffxsi.Tidus, ffxsi.Yuna, ffxsi.Auron, ffxsi.Kimahri, ffxsi.Wakka, ffxsi.Lulu, ffxsi.Rikku };

            stream.Position = 0x6C;
            for (int i = 0; i < chars.Length; i++)
                bw.Write(chars[i].AffectionToTidus);

            stream.Position = 0x5610;
            for (int i = 0; i < chars.Length; i++)
            {
                bw.Write(chars[i].BaseHP);
                bw.Write(chars[i].BaseMP);
                bw.Write(chars[i].BaseStrength);
                bw.Write(chars[i].BaseDefense);
                bw.Write(chars[i].BaseMagic);
                bw.Write(chars[i].BaseMagicDefense);
                bw.Write(chars[i].BaseAgility);
                bw.Write(chars[i].BaseLuck);
                bw.Write(chars[i].BaseEvasion);
                bw.Write(chars[i].BaseAccuracy);

                stream.Position += 4;
                bw.Write(chars[i].AP);
                bw.Write(chars[i].TotalHP); // Current HP
                bw.Write(chars[i].TotalMP); // Current MP
                bw.Write(chars[i].TotalHP); // Current Max HP
                bw.Write(chars[i].TotalMP); // Current Max MP

                stream.Position += 3;
                bw.Write(chars[i].TotalStrength);
                bw.Write(chars[i].TotalDefense);
                bw.Write(chars[i].TotalMagic);
                bw.Write(chars[i].TotalMagicDefense);
                bw.Write(chars[i].TotalAgility);
                bw.Write(chars[i].TotalLuck);
                bw.Write(chars[i].TotalEvasion);
                bw.Write(chars[i].TotalAccuracy);

                bw.Write(chars[i].PercentDamageFromPoison);
                stream.Position++;
                bw.Write(chars[i].CurrentOverdriveGauge);
                bw.Write(chars[i].MaxOverdriveGauge);
                bw.Write(chars[i].CurrentSphereLevel);
                bw.Write(chars[i].TotalSphereLevel);

                stream.Position += 0x17;
                bw.Write(chars[i].EnemiesDefeated);

                stream.Position += 8;
                bw.Write(chars[i].TillWarriorOD);
                bw.Write(chars[i].TillComradeOD);
                bw.Write(new byte[] { 0, 0 }); // Stoic overdrive, ignore this
                bw.Write(chars[i].TillHealerOD);
                bw.Write(chars[i].TillTacticianOD);
                bw.Write(chars[i].TillVictimOD);
                bw.Write(chars[i].TillDancerOD);
                bw.Write(chars[i].TillAvengerOD);
                bw.Write(chars[i].TillSlayerOD);
                bw.Write(chars[i].TillHeroOD);
                bw.Write(chars[i].TillRookOD);
                bw.Write(chars[i].TillVictorOD);
                bw.Write(chars[i].TillCowardOD);
                bw.Write(chars[i].TillAllyOD);
                bw.Write(chars[i].TillSuffererOD);
                bw.Write(chars[i].TillDaredevilOD);
                bw.Write(chars[i].TillLonerOD);

                stream.Position += 6;
                {
                    chars[i].LearnedODModes = 0;

                    if (chars[i].TillWarriorOD == 0)
                        chars[i].LearnedODModes += 0x1;
                    if (chars[i].TillComradeOD == 0)
                        chars[i].LearnedODModes += 0x2;

                    chars[i].LearnedODModes += 0x4; // Stoic is default

                    if (chars[i].TillHealerOD == 0)
                        chars[i].LearnedODModes += 0x8;
                    if (chars[i].TillTacticianOD == 0)
                        chars[i].LearnedODModes += 0x10;
                    if (chars[i].TillVictimOD == 0)
                        chars[i].LearnedODModes += 0x20;
                    if (chars[i].TillDancerOD == 0)
                        chars[i].LearnedODModes += 0x40;
                    if (chars[i].TillAvengerOD == 0)
                        chars[i].LearnedODModes += 0x80;

                    if (chars[i].TillSlayerOD == 0)
                        chars[i].LearnedODModes += 0x100;
                    if (chars[i].TillHeroOD == 0)
                        chars[i].LearnedODModes += 0x200;
                    if (chars[i].TillRookOD == 0)
                        chars[i].LearnedODModes += 0x400;
                    if (chars[i].TillVictorOD == 0)
                        chars[i].LearnedODModes += 0x800;
                    if (chars[i].TillCowardOD == 0)
                        chars[i].LearnedODModes += 0x1000;
                    if (chars[i].TillAllyOD == 0)
                        chars[i].LearnedODModes += 0x2000;
                    if (chars[i].TillSuffererOD == 0)
                        chars[i].LearnedODModes += 0x4000;
                    if (chars[i].TillDaredevilOD == 0)
                        chars[i].LearnedODModes += 0x8000;

                    if (chars[i].TillLonerOD == 0)
                        chars[i].LearnedODModes += 0x10000;

                    if (chars[i].AeonsOnlyOD)
                        chars[i].LearnedODModes += 0x80000;
                }

                bw.Write(chars[i].LearnedODModes);

                stream.Position += 0xC;
            }
        }

        private void SaveItemsData(MemoryStream stream, BinaryWriter bw)
        {
            ListItem itemList = new ListItem();

            stream.Position = 0x3F0C;

            for (int i = 0; i < _inventory1.Count; i++)
            {
                bw.Write(_inventory1[i].Item);
                byte emptyInventoryEntry1 = (byte)(_inventory1[i].Item == 0xFF ? 0x00 : 0x20);
                bw.Write(emptyInventoryEntry1);

                bw.Write(_inventory2[i].Item);
                byte emptyInventoryEntry2 = (byte)(_inventory2[i].Item == 0xFF ? 0x00 : 0x20);
                bw.Write(emptyInventoryEntry2);
            }

            stream.Position = 0x410C;

            for (int i = 0; i < _inventory1.Count; i++)
            {
                byte amount1 = (byte)(_inventory1[i].Item == 0xFF ? 0 : _inventory1[i].Quantity);
                bw.Write(amount1);

                byte amount2 = (byte)(_inventory2[i].Item == 0xFF ? 0 : _inventory2[i].Quantity);
                bw.Write(amount2);
            }
        }

        private void SaveKeyItemsData(MemoryStream stream, BinaryWriter bw)
        {
            ListKeyItem keyItemList = new ListKeyItem();
            keyItemList.InternalOrder();

            KeyItem[] keyItems = new KeyItem[keyItemList.Count];
            for (int i = 0; i < keyItems.Length; i++)
                keyItems[i] = new KeyItem();

            for (int i = 0; i < keyItems.Length; i++)
            {
                foreach (var keyItem1 in _kiInventory1)
                {
                    if (keyItemList[i] == keyItem1.Item)
                    {
                        keyItems[i] = keyItem1;
                        break;
                    }
                }

                foreach (var keyItem2 in _kiInventory2)
                {
                    if (keyItemList[i] == keyItem2.Item)
                    {
                        keyItems[i] = keyItem2;
                        break;
                    }
                }
            }

            if (keyItems[2].Bit && keyItems[3].Bit) // BOTH cloudy mirror and celestial mirror? Can only hold one, so keep the celestial only
                keyItems[2].Bit = false;

            byte val;

            // Withered Bouquet, Flint
            stream.Position = 0x3AC;
            val = (byte)(keyItems[0].Bit ? 1 : 0);
            bw.Write(val);
            val = (byte)(keyItems[1].Bit ? 1 : 0);
            bw.Write(val);

            // Celestial Mirror
            stream.Position = 0x2C6;
            val = (byte)(keyItems[3].Bit ? 0x1C : 0);
            bw.Write(val);
            stream.Position = 0xC78;
            val = (byte)(keyItems[3].Bit ? 1 : 0);
            bw.Write(val);
            stream.Position = 0xCC7;
            val = (byte)(keyItems[2].Bit || keyItems[3].Bit ? 2 : 0); // Cloudy Mirror...
            bw.Write(val);

            // Al Bhed Volumes -- how many we have in total
            stream.Position = 0xC8C;
            val = 0;
            for (int i = 4; i < 30; i++)
                if (keyItems[i].Bit)
                    val++;
            bw.Write(val);

            // Rusty Sword
            stream.Position = 0x2BA;
            val = (byte)(keyItems[33].Bit ? 8 : 0);
            bw.Write(val);

            stream.Position = 0x3D50;
            BitShiftSaveAlBhed(bw, keyItems);

            stream.Position = 0x44CC;
            BitShiftSave(bw, keyItems);
        }

        private void SavePlayersData(MemoryStream stream, BinaryWriter bw)
        {
            BlitzPlayer[] players = { ffxsi.TidusB, ffxsi.WakkaB, ffxsi.DattoB, ffxsi.LettyB, ffxsi.JassuB, ffxsi.BottaB, ffxsi.KeepaB };

            stream.Position = 0x122C;
            for (int i = 0; i < players.Length; i++)
            {
                uint offensiveTechniques = (players[i].LearnedTechniques == (0xFEFFFF7FL * 2) - 0x100000000) ? 0xFEFFFF7F : players[i].OffensiveTechniques;
                byte[] offensiveTechniquesBA = BitConverter.GetBytes(offensiveTechniques);
                Array.Reverse(offensiveTechniquesBA);
                bw.Write(offensiveTechniquesBA);
            }

            stream.Position = 0x131C;
            for (int i = 0; i < players.Length; i++)
            {
                uint defensiveTechniques = (players[i].LearnedTechniques == (0xFEFFFF7FL * 2) - 0x100000000) ? 0xFEFFFF7F : players[i].DefensiveTechniques;
                byte[] defensiveTechniquesBA = BitConverter.GetBytes(defensiveTechniques);
                Array.Reverse(defensiveTechniquesBA);
                bw.Write(defensiveTechniquesBA);
            }

            stream.Position = 0x15BE;
            for (int i = 0; i < players.Length; i++)
                bw.Write(players[i].TechniqueSlots);

            stream.Position = 0x15FA;
            for (int i = 0; i < players.Length; i++)
                bw.Write(players[i].Level);

            stream.Position = 0x1756;
            for (int i = 0; i < players.Length; i++)
                bw.Write(players[i].ContractGamesLeft);

            stream.Position = 0x1794;
            for (int i = 0; i < players.Length; i++)
                bw.Write(players[i].Experience);
        }

        private void yojimboOptionComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as ComboBox).SelectedItem != null)
                switch ((sender as ComboBox).SelectedItem.ToString())
                {
                    case "To train as a summoner.":
                        ffxsi.YojimboOption = 0;
                        break;

                    case "To gain the power to destroy fiends.":
                        ffxsi.YojimboOption = 1;
                        break;

                    case "To defeat the most powerful of enemies.":
                        ffxsi.YojimboOption = 2;
                        break;
                }
        }
    }
}
